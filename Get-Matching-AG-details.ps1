param (
        [Parameter(Mandatory = $True)]
        [System.String]$match_term = "NULL",
        [Parameter(Mandatory = $True)]
        [System.String]$show_attributes = "ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS",
        [Parameter(Mandatory = $True)]
        [Microsoft.PowerShell.Commands.WebRequestSession]$cookie,
        [Parameter(Mandatory = $True)]
        $script_home = $(Split-Path -Path $MyInvocation.MyCommand.Definition -Paren),
        [Parameter(Mandatory = $True)]
        $InputFile = "$script_home/firewall/check_ag.csv"
)

$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$asset_group_api = "asset/group"
# $all_asset_titles_id = "action=list&show_attributes=TITLE&output_format=csv"

# $selected_xyz_asset_titles_body = "action=list&ids=$my_xyzids&show_attributes=ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS&output_format=csv"
# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $selected_xyz_asset_titles_body -WebSession $sess -OutFile "$script_home/firewall/selected_xyz_check_ag.csv"

# $all_asset_titles_id = "action=list&show_attributes=$show_attributes&output_format=csv"
# $selected_asset_titles_id = "action=list&ids=$ag_id_list&show_attributes=$show_attributes&output_format=csv"
# $select_group =  New-Object 'System.Collections.Generic.List[String]' 
##### Available data fields for asset groups
### ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
### HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME
#PS D:\git\qualys_ag_auto> $session_out = .\Get-Qualys-Session.ps1 -QualysUserName sunet2ma -QualysPassword $QualysPassword -Action login
#PS D:\git\qualys_ag_auto> $cookie = $session_out[1]
#PS D:\git\qualys_ag_auto> Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $all_asset_titles_id -WebSession $cookie -OutFile "D:\git\qualys_ag_auto\test_outfile.txt"
#PS D:\git\qualys_ag_auto> .\Get-Qualys-Session.ps1 -QualysUserName sunet2ma -QualysPassword $QualysPassword -Action logout -logout_sess $cookie

$DebugPreference = "Continue"
# [Microsoft.PowerShell.Commands.WebRequestSession]$sess=$null
# [Microsoft.PowerShell.Commands.WebRequestSession]$session=$null

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser
# Clear-Host
if ($PSVersionTable.PSVersion.Major -lt 5)
{
        Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
        exit
}
else
{
        Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}

function assemble_ag_ids
{
        param
        (
                [Parameter(Mandatory = $true)]
                $match_term,
                [Parameter(Mandatory = $true)]
                $InputFile
        )
}


function get_qualys_aglist
{
        param
        (
                [Parameter(Mandatory = $true)]
                [Microsoft.PowerShell.Commands.WebRequestSession] $cookie,
                [Parameter(Mandatory = $true)]
                $body_value
        )

    # Get Asset Group list into a csv file.
    Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $body_value -WebSession $cookie -OutFile "$script_home/firewall/selected_check_ag.csv"
	#return [Microsoft.PowerShell.Commands.WebRequestSession]$session
    # return $session
}




$QualysPlatform,$HttpHeaders,$creds,$login_action=D:\git\qualys_ag_auto\Set-Qualys-Variables.ps1 -id $QualysUserName -pwd $QualysPassword

# return $sess

# Write-Debug "QualysPlatform = $QualysPlatform"

if ($match_term -eq "NULL") {
   # [Microsoft.PowerShell.Commands.WebRequestSession]$session = get_qualys_session
   $body_value = "action=list&show_attributes=$show_attributes&output_format=csv"
} else {
   # Write-Debug "ag_id list = $ag_id_list"
   $body_value = "action=list&ids=$ag_id_list&show_attributes=$show_attributes&output_format=csv"
}
get_qualys_aglist -cookie $cookie -body_value $body_value