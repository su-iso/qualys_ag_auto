#! /usr/bin/python -v
from gsheets import Sheets
sheets = Sheets.from_files('.\iso-qualys-sheets-oauth.json', '.\storage.json')
url = 'https://docs.google.com/spreadsheets/d/1YfzmzNs-7taIQooo7yylSvmRF1M0CqQtUnHGV_idqIc'
s = sheets.get(url)
s.sheets[0].to_csv('.\q-by-bu-current.csv', encoding='utf-8', dialect='excel')
print("All done!")
