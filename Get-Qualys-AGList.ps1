param (
        [Parameter(Mandatory = $False)]
        [System.String]$ag_id_list = "NULL",
        [Parameter(Mandatory = $True)]
        [System.String]$show_attributes = "TITLE",
        [Parameter(Mandatory = $True)]
        [Microsoft.PowerShell.Commands.WebRequestSession]$cookie,
        [Parameter(Mandatory = $true)]
        [System.Management.Automation.PSCredential]$QualysCredential
)

$script_home = "d:\git\qualys_ag_auto"
$asset_group_api = "asset/group"


$DebugPreference = "Continue"
# [Microsoft.PowerShell.Commands.WebRequestSession]$sess=$null

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

if ($PSVersionTable.PSVersion.Major -lt 5)
{
        Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
        exit
}
else
{
        # Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}


function get_qualys_aglist
{
        param
        (
                [Parameter(Mandatory = $true)]
                [Microsoft.PowerShell.Commands.WebRequestSession] $cookie,
                [Parameter(Mandatory = $true)]
                $body_value
        )

    # $filedest = $null

    # New-Item "$script_home/firewall/csv" -ItemType directory

    # Get Asset Group list into a csv file.
    $date = $(Get-Date -UFormat "%Y%m%d%H%M%S")


    $tmp_filename = "$script_home\firewall\$date.csv"
        
    Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $body_value -WebSession $cookie -OutFile "$tmp_filename"
	#return [Microsoft.PowerShell.Commands.WebRequestSession]$session
    $filehash = (Get-FileHash -Path $tmp_filename -Algorithm MD5).Hash


    if (!(Test-Path -Path $script_home/firewall/csv/$filehash/)) {
        $snarf = New-Item -Path "$script_home/firewall/csv/$filehash" -ItemType directory
        Move-Item -Path $tmp_filename -Destination "$script_home\firewall\csv\$filehash\"
    } else {
        Move-Item -Path $tmp_filename -Destination "$script_home\firewall\csv\$filehash\"
    }    
    $agfilename = $null
    $agfilename = Get-ChildItem -Path "$script_home\firewall\csv\$filehash" -File |Sort-Object LastAccessTime -Descending| Select-Object -First 1
    # Write-Host "FFS:  $agfilename" 
    # $moo = $agfilename.FullName
    # Write-Host "Moo $moo"
    return $agfilename
}

# $QualysPlatform,$HttpHeaders,$login_action,$logout_action=D:\git\qualys_ag_auto\Set-Qualys-Variables.ps1 -QualysUserName $QualysUserName -QualysPassword $QualysPassword
$QualysPlatform,$HttpHeaders,$login_action,$logout_action=D:\git\qualys_ag_auto\Set-Qualys-Variables.ps1 -QualysCredential $QualysCredential
# return $sess

# Write-Debug "QualysPlatform = $QualysPlatform"

if ($ag_id_list -eq "NULL") {
   # [Microsoft.PowerShell.Commands.WebRequestSession]$session = get_qualys_session
   $body_value = "action=list&show_attributes=$show_attributes&output_format=csv"
} else {
   # Write-Debug "ag_id list = $ag_id_list"
   $body_value = "action=list&ids=$ag_id_list&show_attributes=$show_attributes&output_format=csv"
}


#if (Test-Path $script_home/firewall/csv){
#    Remove-Item "$script_home/firewall/csv" -Recurse
#    New-Item "$script_home/firewall/csv" -ItemType directory -Force
#} else {
#    New-Item "$script_home/firewall/csv" -ItemType directory -Force
#}

get_qualys_aglist -cookie $cookie -body_value $body_value
# Write-Host "Boo $filedest"
