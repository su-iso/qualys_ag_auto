# FOA-srtr:  ASIA-PROD (vsys80), STG-MGM (vsys4), PSOFT-UAT (vsys55), BIGFIX (vsys5)
$foa2_vsys=@(80,4,55,5)
$header = "Address Space", "First IP", "LastIP", "OA", "Vlan", "Firewall", "Interface", "Vsys", "zone", "Buildings"
$DebugPreference = "Continue"
# $script_home = "/home/netdb/qualys/"
$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$mydate_main = $(Get-Date -Format s)

. "./$script_homei/Get-IPrange.ps1"
# FOA2-srtr: BACKUP-SRV (vsys32)
$foa_vsys=@(32)

$summary_list = New-Object 'System.Collections.Generic.List[String]'
$sumlist = @("agname","tmp_count","to_remove_Count","check_new_vs_old_Count","check_overlap_Count")
# $summary_list.Add($sumlist)
$sumlist -join "," >> "$script_home/summary_$mydate_main.csv" 
# $sumlist >> "$script_home/summary_$mydate_main.csv" 

$netfile = "$script_home/net2oa.txt"
$file = Get-Content -Path $netfile
$filehash = (Get-FileHash -Path $netfile -Algorithm MD5).Hash
$fwdir = "$script_home/firewall/$filehash" 

if (!(Test-Path -path $fwdir)) {

# Write-Debug "### Filehash:  $filehash"
New-Item "$script_home/firewall/$filehash" -ItemType directory
New-Item "$script_home/firewall/$filehash/upload" -ItemType directory

$drop_header = $file[1..($file.Count-1)]
Remove-Item "$script_home/content.csv" -Force
# newly added since last run
$drop_header | Select-String -Pattern "1.1.1.0/26" -NotMatch > "$script_home/content.csv"
$content = Import-Csv "$script_home/content.csv" -Header $header -Delimiter "|"

# Remove and recreate 
# Remove-Item "$script_home/firewall/$filehash" -Force -Recurse

# Send CIDR block to a file named firewall/$filehash/<firewall_name>_<vsys>.txt
  $content | ForEach-Object {
    $fw=$_.Firewall
    $vsys=$_.Vsys.ToInt32($null)
    $addr=$_.'Address Space'
    $filename="null"
    # Write-Debug "$fw, Avsys_inside:  $vsys"
    if ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $foa_vsys.Contains($vsys))
    {
        # Write-Debug "AOutput:$fw, $vsys"
        $filename="special"+$fw+"-"+$vsys+".txt"
        $filename_fw="upload/x-special-"+$fw+".txt"
        Write-Debug "filename:  $filename"
    } elseif ($fw -like "foa2*" -and $foa2_vsys.Contains($vsys)) {
        # Write-Debug "bOutput:$fw, $vsys"
        $filename="special"+$fw+"-"+$vsys+".txt"
        $filename_fw="upload/x-special-"+$fw+".txt"
        # Write-Debug "BOutput:$fw, $vsys"
    } else {
        $filename=$fw+"-"+$vsys+".txt"
        $filename_fw="x-"+$fw+".MASTER.txt"
	## TODO: Add a lookup table for fw > OA mapping where it matters, so that we can do roll-ups and sub-groups
    }
    $addr | Out-File -NoClobber -Append -FilePath "$script_home/firewall/$filehash/$filename" -Encoding ascii
    # Write-Debug "TEST:  $script_home/firewall/$filehash/$filename_fw" 
    $addr | Out-File -NoClobber -Append -FilePath "$script_home/firewall/$filehash/$filename_fw" -Encoding ascii
    # $addr | Out-File -NoClobber -Append -FilePath "$script_home/firewall/$filehash/filename_fw" -Encoding ascii
  }
} else {
# $LastDate = 
Write-Debug "Directory $fwdir already exists, exiting."
}


# $all_oas=@("dcs", "nw", "foa", "foa2", "doa", "lb", "loa", "moa", "noa", "nc", "rcf", "sc", "woa", "wc")
$all_oas=@("dcs", "foa", "foa2", "doa", "lb", "loa", "moa", "noa", "nc", "rcf", "sc", "woa", "wc")
$oa_to_split=@("foa","foa2","mc","nc","doa","moa","noa")



foreach ($oa in $all_oas) {
	$master_filename="x-"+$oa+"-srtr.MASTER.txt"
	# $master_agname = "x-"+$oa+"-srtr.MASTER"
	$countpath = $("$script_home/firewall/$filehash/"+$master_filename)
	# Write-Debug "Countpath:  $countpath"
	# Get count of lines
	$countlines=$(Get-Content -path $countpath|Measure-Object -Line).Lines
	# Write-Debug "OA $oa has $countlines for whee"

	if ($oa_to_split -contains $oa ) {
	    $i = $null
	    $j = 1
	    $total = 0
	    # Write-Debug "countlines:  $countlines"
	    $num_groups = 10
	    $groupsize = $countlines/$num_groups
	    $groupsize1 = $groupsize.ToString().Split('.')[0]
	    $groupsize = [int]$groupsize1
	    $last_group = $countlines%$num_groups

	    $max_count = $countlines
	    $max_per_pass = [int]$($max_count / $num_groups)
	    # Write-Debug "Max_per_pass: $max_per_pass"
	    $checklines=$(Get-Content -Path $countpath)
	    # $checklines | Out-File -Path $("$script_home/firewall/$filehash/upload/$master_filename")
	    $checklines > "$script_home/firewall/$filehash/upload/$master_filename"
	    $list = New-Object 'System.Collections.Generic.List[String]'
	    ForEach ($row in $checklines) {
		$list.Add($row)
		$i++
		$total++
		if ($i -eq $max_per_pass -AND $j -lt $num_groups) {
			# $writepath = "$script_home/firewall/$filehash/"+$oa+".upload-"+$j+".txt"
			$writepath = "$script_home/firewall/$filehash/upload/x-"+$oa+"-group-"+$j+".txt"
			Out-File -FilePath $writepath -encoding ascii -inputobject $list
			$list = New-Object 'System.Collections.Generic.List[String]'
			$i = $null
			$j++
		} elseif ($j -eq $num_groups -AND $total -le $max_count) {
			# Write-Debug "total:$total"
			# Write-Debug "j:$j"
			# Write-Debug "row:$row"
			$writepath = "$script_home/firewall/$filehash/upload/x-"+$oa+"-group-"+$j+".txt"
			Out-File -FilePath $writepath -encoding ascii -inputobject $list
			}
		}
	    } else {
	        $checklines=$(Get-Content -Path $countpath)
	        # $checklines > "$script_home/firewall/$filehash/upload/"+$master_filename
		$writepath = "$script_home/firewall/$filehash/upload/x-"+$oa+".txt"
		# Write-Debug "Writepath:$writepath"
		# Write-Debug "oa:$oa"
		Out-File -FilePath $writepath -encoding ascii -inputobject $checklines
		   }

	}

# function Set-AG
# {
#     $asdf
# }

# function Get-AG
# {
#     $asdf
# }

# new asset group:
#  $new_asset_title = "x_specialfoa-srtr_32"
#  $ips = 171.67.40.176/29,171.67.40.184/29,172.27.40.176/29,172.27.40.184/29
#  $ips = '171.67.40.176/29,171.67.40.184/29,172.27.40.176/29,172.27.40.184/29'
#  $add_asset_group_body = "action=add&title=$new_asset_title&ips=$ips"
#  Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $add_asset_group_body -WebSession ...
# 


# Get a qualys session, get a list of asset groups, destroy the session



# Set up qualys API login info.   NOTE these first two lines will be different per group.
$QualysUserName = $(get-Content -Path "$script_home/qualysuser.txt")
$QualysPassword = $(get-Content -Path "$script_home/qualyspw.txt")

$QualysPlatform = 'https://qualysapi.qg2.apps.qualys.com/api/2.0/fo'
$BasicAuthString = [System.Text.Encoding]::UTF8.GetBytes("$QualysUserName`:$QualysPassword")

# $BasicAuthBase64Encoded = [System.Convert]::ToBase64String($BasicAuthString)
$password_base64 = ConvertTo-SecureString $QualysPassword -AsPlainText -Force  
$creds = New-Object System.Management.Automation.PSCredential ($QualysUserName, $password_base64) 

# $BasicAuthFormedCredential = "Basic $BasicAuthBase64Encoded"
$BasicAuthFormedCredential = "Basic $creds"
# $HttpHeaders = @{'X-Requested-With' = 'PowerShell Script'}
$HttpHeaders = @{'Authorization' = $BasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}


# API actions and endpoints
$login_action = "action=login&username=$QualysUserName&password=$QualysPassword"
$logout_action = "action=logout"

$asset_group_api = "asset/group"
$all_asset_titles_id = "action=list&show_attributes=TITLE&output_format=csv"
$select_group =  New-Object 'System.Collections.Generic.List[String]' 

# Messy, might not be usable as is.
# $all_assets_fulltuple = "action=list&show_attributes=TITLE,ID,IP_SET,APPLIANCE_LIST,COMMENTS&output_format=csv"

##### Available data fields for asset groups
### ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
### HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME

# Log in to Qualys.
# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $login_action -SessionVariable sess
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Credential $creds -Body $login_action -SessionVariable sess

# Get Asset Group list into a csv file.
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $all_asset_titles_id -WebSession $sess -OutFile "$script_home/check_ag.csv"
# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $all_assets_fulltuple -WebSession $sess -OutFile "$script_home/check_ag_fulltuple.csv"



$check_csv_full = $(Get-Content -Path "$script_home/check_ag.csv")

$my_x_groups =  New-Object 'System.Collections.Generic.List[String]' 
$my_y_groups =  New-Object 'System.Collections.Generic.List[String]' 
$my_z_groups =  New-Object 'System.Collections.Generic.List[String]' 
$my_x_objects = New-Object 'System.Collections.ArrayList'
$my_x_ids = New-Object 'System.Collections.Generic.List[String]' 
$my_x_fullobjects = New-Object 'System.Collections.ArrayList'


# First output line is the indicator of success!
$lineone = $check_csv_full[0]
Write-Debug "check_csv_full[0]: $lineone"
if ($lineone -like "----BEGIN_RESPONSE_BODY_CSV") 
   { 
   Write-Debug "Check_csv_test"
   # Second line is the header
   # $check_csv_header = $check_csv_full[1]
   # $check_csv_header = $($check_csv_full[1].Split(',').Replace("`"",""))
   # $get_csv_content = $check_csv_full[2..($check_csv_full.Count-2)]

   # Get all lines from the output but the first two and the last one 
   # This approach includes the header line, which is usually line 2 of the response at least for asset group output
   $check_csv_full[1..($check_csv_full.Count-2)] > "$script_home/response_content.csv"
   # $response_content becomes the reference point for all AG > ID lookups
   $response_content = Import-Csv "$script_home/response_content.csv" 
   
   $response_content | ForEach-Object {
			$id=$_.ID
			$title=$_.TITLE
			if ($title -like "z-*") {
				# Write-Debug "$title,$id" 
				$my_z_groups.Add("$title,$id")
				}
			elseif ($title -like "x-*") {
				Write-Debug "my_x_groups:  $title,$id" 
				$my_x_groups.Add("$title")
				$my_x_ids.Add("$id")
				$my_x_objects.Add("$_")
				}
			# elseif ($title -like "y-*") {
			#	# Write-Debug "my_x_groups:  $title,$id" 
			#	$my_y_groups.Add("$title,$id")
			#	}
			}
  # Get the extended data just for the Asset Groups we care about
  $my_groups = $my_z_groups -join ","
  $my_zgroups = $my_z_groups -join ","
  $my_xgroups = $my_x_groups -join ","
  $my_ygroups = $my_y_groups -join ","
  $my_xids = $my_x_ids -join ","


  ####################################################
  # Update this whole area to allow for a select by {x,y,z} as well as probably other things.
  #
  ################
  ###-----> Valid attributes for request of Asset groups in Qualys v2 api:  
  ###
  # ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
  # HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME
  #
  ####################################################

  $selected_x_asset_titles_body = "action=list&ids=$my_xids&show_attributes=ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS&output_format=csv"


  # Write-Debug "#### CURRENT:  my_x_groups:  $my_x_groups"
  # Write-Debug "Get info for selected asset groups"
  Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $selected_x_asset_titles_body -WebSession $sess -OutFile "$script_home/selected_x_check_ag.csv"
  
  } 



# asdf
# Get the current x-asset group info from csv
$current_x_group_info = $(Get-Content -Path "$script_home/selected_x_check_ag.csv")
# $current_x_group_info[1..($current_x_group_info.Count-2)] > "$script_home/current_x_group_info.csv"
# Get the header
$current_x_headervals = $current_x_group_info[1]
$current_x_headers = $current_x_headervals.Split(",").Replace("`"","") 
# $current_x_header = $($current_x_header.Replace("`"",""))

# Add columns to the header for overloading
# $add = '"PROPOSED_IP_SET","DELTA","DIFF_FOR_COMMENTS"'


Write-Debug "#### CURRENT:  current_x_headers:  $current_x_headers"


# Get just the content
$current_x_group_info[2..($current_x_group_info.Count-2)] > "$script_home/current_x_group_info.csv"


# Get the header and the content
# $current_x_group_info[1..($current_x_group_info.Count-2)] > "$script_home/current_x_group_info.csv"

$current_x_group_content = Import-Csv "$script_home/current_x_group_info.csv" -Header $current_x_headers -Delimiter ","


# Get list of upload files
$files = $(Get-ChildItem -Force "$script_home/firewall/$filehash/upload" -File) 


# $post_action_aglist = New-Object 'System.Collections.Generic.List[String]'

# check whether that ag name already exists in $response_content, which should be better named
# if it does alredy exist, grab the ID from $response content and save it in an array for using to select to-be-updated AG info into a special array of objects

# for each of the files
$files | ForEach-Object {
	# get asset group name
        $name = $_.Name
	$agname = $_.Name.TrimEnd(".txt")
	if ($agname -contains "MASTER") {
        	Write-Debug "AG_name: ----------------------------------------- $agname contains MASTER"
        	$agname = $agname.TrimEnd(".MASTER")
		}
	# and proposed IP addresses
        $content = Get-Content -Path "$script_home/firewall/$filehash/upload/$name" 
        Write-Debug "AG_name: $agname"
        # $ips = $($content -join ",")

	$new_ips_expanded = New-Object 'System.Collections.Generic.List[String]'

	# Assign the values (CIDR blocks) from the individual network files to the new_ips_expanded list as expanded ranges
	$content| ForEach-Object {(Invoke-PSipcalc -NetworkAddress $_ -Enumerate).IPEnumerated|ForEach-Object {$new_ips_expanded.Add($_)}}
	
	$ips_count=$new_ips_expanded.Count

	$ips=$new_ips_expanded -join ","
	$agid = $null
	
	# Check whether AG already exists
	# $mx_x_groups is a global and a cheat, I'm sorry with a Canadian accent.
	if ($my_x_groups -contains $agname) {
		# If AG already exists, grab its ID
		# It's an edit operation!  We need an asset group ID for this to work.
		# Write-Debug "my_x_objects = $my_x_objects"
		$matching_ag_id = $null
		$old_ips = $null
		$comments = $null
		$existing_ips_expanded = New-Object 'System.Collections.Generic.List[String]'
		# $new_ips| ForEach-Object {(Invoke-PSipcalc -NetworkAddress $_ -Enumerate).IPEnumerated}

		# This will be the whole object which matches the AG name
		$chosen = $($current_x_group_content| Where-Object {$_.TITLE -eq "$agname"})
		$matching_ag_id = $($chosen.ID)
		$agid = $($chosen.ID)
		
		if ($matching_ag_id -ne $null) {
			$old_ips=$($chosen.IP_SET)
			$old_ips.Split(',') | ForEach-Object {
			        # Write-Debug "POSITION ONE:  $_"
				if ($_.contains('-')) {
				   # It's a range
				   $start=$_.Split("-")[0].ToString()
				   $end=$_.Split("-")[1].ToString()
				   # Write-Debug "POSITION ALPHA start: $start; end: $end"
				   $rc = Get-IPrange -start $start -end $end 
				   # Write-Debug "POSITION WTF rc: $rc"
				   # $existing_ips_expanded.Add($rc)
				   $rc | ForEach-Object {
				        $existing_ips_expanded.Add($_)
					# Write-Debug "Position two: $_"
				   }
				   #$list.Add($row)
				} else {
				   # Write-Debug "POSITION BETA: $_"
				   $existing_ips_expanded.Add($_)
				}

			}

		        # $existing_ips_expanded.Add($_)

			# $old_ips_expanded = $old_ips
			# $old_comments=$($chosen.COMMENTS)
			# Write-Debug "Not null!: $($chosen.ID)"
			Write-Debug "###################### UPGRAAAAADE! Name $agname found!!! Existing AG $agname will need to be updated. ID is $matching_ag_id"
			# Write-Debug "IP Set: $old_ips to be written to comments field"
			#Write-Debug "NEW IP Set: $ips"
			# Write-Debug "old_comments:  $old_comments"

			# Using Get-Date for now since the comments field is too small for diffs
			$comments = "Updated:  $(Get-Date -Format s)"
			# if ($ips_and_comments_same -eq True) {
# asdf
			#	
			#}
		}
		# $check_old_vs_new = ?($new_ips_expanded -notcontains $existing_ips_expanded)

		# if there is something to remove, or something in new_vs old, do stuff

		# https://stackoverflow.com/questions/19012457/compare-two-list-and-find-names-that-are-in-list-one-and-not-list-two-using-powe
		$to_remove=$($existing_ips_expanded | ?{$new_ips_expanded -notcontains $_})
		# $comments=$to_remove -join ","
		$check_new_vs_old=$($new_ips_expanded |?{$existing_ips_expanded -notcontains $_})
		$check_overlap=$($new_ips_expanded | ?{$existing_ips_expanded -contains $_})
		# $FolderList | ?{$AdUserName -notcontains $_}

		# 
	
		# Write-Debug "----------- existing_ips_expanded:  $existing_ips_expanded"
		# Write-Debug "----------- new_ips_expanded:  $new_ips_expanded"
		$to_remove_Count=$to_remove.Count
		$check_new_vs_old_Count=$check_new_vs_old.Count
		$check_overlap_Count=$check_overlap.Count
		# Write-Debug "----------- to_remove:  $to_remove"
		if ($to_remove_Count -gt 0 -Or $check_new_vs_old_Count -gt 0 -And $ips_count -gt 0) {
			# Write-Debug "----------- to_remove: $to_remove_Count"
			# Write-Debug "----------- check_new_vs_old: $check_new_vs_old_Count"
			# Write-Debug "----------- total new ips: $ips_count"
			# Write-Debug "----------- check_overlap_Count:  $check_overlap_Count"
			$action_asset_group_body = "action=edit&id=$matching_ag_id&set_ips=$ips&set_comments=$comments"
			# $action_ag_post_check = "action=edit&id=$matching_ag_id&set_ips=$ips&set_comments=$comments"
		} else {
		$action_asset_group_body = "action=list&ids=$matching_ag_id"
		Write-Debug "===== continue - no change section: should only hit here if new, remove, and payload are all 0"
		#continue
		# break
		}
		# Write-Debug "----------- check_overlap: $check_overlap_Count"
		# Write-Debug "action=edit&id=$matching_ag_id&set_ips=$ips&set_comments=$comments"
		} else {
		# Should be a new asset group name
		Write-Debug "Name $agname will be a new AG."
		$action_asset_group_body = "action=add&title=$agname&ips=$ips"
 		}

	$action_aftercheck = "action=list&ids=$agid&show_attributes=ID,TITLE,IP_SET&output_format=csv"


	###### uncomment to actually upload / change asset groups
	# The actual qualys call to add/update the AG
	# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $action_asset_group_body -WebSession $sess -OutFile "$script_home/check_asset_group_add.txt"
	Start-Sleep -s 2


  	Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $action_aftercheck -WebSession $sess -OutFile "$script_home/aftercheck_x.csv"

	$post_info = $(Get-Content -Path "$script_home/aftercheck_x.csv")
	$post_info[2..($post_info.Count-2)] > "$script_home/aftercheck_x_info.csv"
	$post_headervals = $post_info[1]
	$post_x_headers = $post_headervals.Split(",").Replace("`"","")
	$post_x_content = Import-Csv "$script_home/aftercheck_x_info.csv" -Header $current_x_headers -Delimiter ","
	$post_action_aglist = New-Object 'System.Collections.Generic.List[String]'
	$rangeset = New-Object 'System.Collections.Generic.List[String]'
	$ipset = New-Object 'System.Collections.Generic.List[String]'

	# add expanded output.Count from post-check
	$post_x_content | ForEach-Object {
		# $ipset=$_.IP_SET.ToString()
		# $ipset=$_.IP_SET.Split(',')
		$ipset=$_.IP_SET
		# Write-Debug "POSITION ONE:  $ipset.split(',')"
		if ($ipset -notcontains(',')) {
			# Write-Debug "POSITION FUUUUU:  $ipset"
			$rangeset.Add($ipset)
			} else {
			$ipset.Split(',') | ForEach-Object {
			# Write-Debug "POSITION FUUUUU:  $ipset"
			$rangeset.Add($_)
			}
		}
		# Write-Debug "POSITION ONE:  $ipset"
		# Write-Debug "POSITION ONE:  "+$ipset.split(',')
		$rangeset| ForEach-Object {
			if ($_.contains('-')) {
			   $start=$_.Split("-")[0].ToString()
			   $end=$_.Split("-")[1].ToString()
			   # Write-Debug "POSITION ALPHA start: $start; end: $end"
			   $rc = Get-IPrange -start $start -end $end
			   # Write-Debug "POSITION WTF rc: $rc"
			   # $existing_ips_expanded.Add($rc)
			   $rc | ForEach-Object {
				$post_action_aglist.Add($_)
				# Write-Debug "Position two: $_"
				}
			} else {
		   # Write-Debug "POSITION BETA: $_"
		   # $existing_ips_expanded.Add($_.IP_SET)
		   $post_action_aglist.Add($_.IP_SET)
		}

	}
	$tmp_count=$post_action_aglist.Count
        # Write-Debug "post_x_content $agname, ip count: $tmp_count"
	# Write-Debug "#######----------- to_remove: $to_remove_Count"
	$this_round = @($agname,$tmp_count,$to_remove_Count,$check_new_vs_old_Count,$check_overlap_Count)
	$summary_list.Add("$this_round")
	#$HttpHeaders = @{'Authorization' = $BasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}
	# Write-Debug "Summary list: $summary_list"
	# $summary_list >> "$script_home/summary_$mydate_main.csv" 
	$this_round -join "," >> "$script_home/summary_$mydate_main.csv" 
	}
}

# Log out of Qualys.
Write-Debug "Logging out of Qualys"
# Write-Debug "logout_action:  $logout_action"
# Write-Debug "sess:  $sess"

$mydate = $(Get-Date -Format s)

# asdf
# $summary_list | Export-Csv -Path "$script_home/summary_$mydate.csv" 

# Get the post-upload data for changed asset groups
# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $all_assets_fulltuple -WebSession $sess -OutFile "$script_home/check_ag_fulltuple.csv"

Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -creds $creds -Body $logout_action -WebSession $sess
# QualysSessionStartEnd "logout"
# # Log in to Qualys.
# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $login_action -SessionVariable sess

# 
# # Log out of Qualys.
# Write-Debug "Logging out of Qualys"
# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $sess

