
import os
import sys
import ipaddress
import csv
import re
import shutil

all_nets_file = 'd:/git/qualys_ag_auto/all_nets_uniq.txt'
input_file = 'd:/git/qualys_ag_auto/net2oa.txt'

firewalled_nets = list()

with open(input_file, 'r', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter='|', quotechar='\'')
    for row in reader:
        a_space = row['#Address Space']
        firewalled_nets.append(a_space)

output_file = 'd:/git/qualys_ag_auto/fwnets.txt'

with open(output_file,'w', newline='') as filehandle:
    for fwnet in firewalled_nets:
        filehandle.write('%s\n' % fwnet)


all_range = []
firewalled_ranges = []

all_nets = open(all_nets_file)
f_all_nets = all_nets.read()
lines = f_all_nets.split('\n')

for i in lines:
    if i.count(".") == 3:
        net = ipaddress.IPv4Network(i)
        all_range.append(net)

fw_nets = open(output_file)
firewall_nets = fw_nets.read()
flines = firewall_nets.split('\n')
for i in flines:
    if i.count(".") == 3:
        net = ipaddress.IPv4Network(i)
        firewalled_ranges.append(net)
        
for range in list(firewalled_ranges):
    for net in list(all_range):
        if net.supernet_of(range):
            all_range.remove(net)
            new_net = list(net.address_exclude(range))
            for newnet in list(new_net):
                all_range.append(newnet)
            continue

non_fwnets_output_file = 'd:/git/qualys_ag_auto/non_fwnets_output.txt'

dot_10 = re.compile("10.")
dot_128 = re.compile("128.")
dot_171 = re.compile("171.")
dot_172 = re.compile("172.")
dot_192 = re.compile("192.")
dot_204 = re.compile("204.")

uf_10 = []
uf_128 = []
uf_171 = []
uf_172 = []
uf_192 = []
uf_204 = []
uf_unknown = []

with open(non_fwnets_output_file,'w', newline='') as filehandle:
    for allnet in all_range:
        net = str(allnet)
        filehandle.write("%s \n" % net)
        if dot_10.match(net):
            uf_10.append(net)
        elif dot_128.match(net):
            uf_128.append(net)
        elif dot_171.match(net):
            uf_171.append(net)
        elif dot_172.match(net):
            uf_172.append(net)
        elif dot_192.match(net):
            uf_192.append(net)
        elif dot_204.match(net):
            uf_204.append(net)
        else: 
            uf_unknown.append(net)


shutil.rmtree("d:/git/qualys_ag_auto/firewall/unfirewalled",ignore_errors=True)
# os.rmdir("d:/git/qualys_ag_auto/firewall/unfirewalled/")
# os.mkdir("d:/git/qualys_ag_auto/firewall/unfirewalled")
os.mkdir("d:/git/qualys_ag_auto/firewall/unfirewalled")
uf_10_outfile = 'd:/git/qualys_ag_auto/firewall/unfirewalled/x-10-unfirewalled.txt'
uf_128_outfile = 'd:/git/qualys_ag_auto/firewall/unfirewalled/x-128-unfirewalled.txt'
uf_171_outfile = 'd:/git/qualys_ag_auto/firewall/unfirewalled/x-171-unfirewalled.txt'
uf_172_outfile = 'd:/git/qualys_ag_auto/firewall/unfirewalled/x-172-unfirewalled.txt'
uf_192_outfile = 'd:/git/qualys_ag_auto/firewall/unfirewalled/x-192-unfirewalled.txt'
uf_204_outfile = 'd:/git/qualys_ag_auto/firewall/unfirewalled/x-204-unfirewalled.txt'

with open(uf_10_outfile,'w', newline=None) as filehandle:
    for net in uf_10:
        filehandle.write("%s\n" % net)

with open(uf_128_outfile,'w', newline=None) as filehandle:
    for net in uf_128:
        filehandle.write("%s\n" % net)
        # filehandle.write('\n' % net)

with open(uf_171_outfile,'w', newline=None) as filehandle:
    for net in uf_171:
        filehandle.write("%s\n" % net)

with open(uf_172_outfile,'w', newline=None) as filehandle:
    for net in uf_172:
        filehandle.write("%s\n" % net)

with open(uf_192_outfile,'w', newline=None) as filehandle:
    for net in uf_192:
        filehandle.write("%s\n" % net)

with open(uf_204_outfile,'w', newline=None) as filehandle:
    for net in uf_204:
        filehandle.write("%s\n" % net)

