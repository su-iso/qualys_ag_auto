#

param(
[string]$input_file="d:\git\qualys_ag_auto\q-by-bu-current.csv",
[Int]$iso_mode=0,
[string]$contact_list=@('tsimpson@stanford.edu')
)

$DebugPreference = "Continue"

# Set $script_home to the script's parent's path
$script_home = "d:\git\qualys_ag_auto"
$mydate_main = $(Get-Date -Format s)
# Remove-Item "$script_home/reports" -Force -Recurse
# New-Item "$script_home/reports" -ItemType directory -Force

$credsfile = "D:\git\qualys_ag_auto\qualys_creds.xml"
$creds = Import-Clixml -Path $credsfile
$QualysPassword = $creds.GetNetworkCredential().Password
$QualysUsername = $creds.UserName
$QualysPlatform,$HttpHeaders,$login_action = .\Set-Qualys-Variables.ps1 -QualysCredential $creds
$session = .\Get-Qualys-Session.ps1 -QualysCredential $creds -Action login
$cookie = $session[1]

$report_api = "report"
$list_action = "action=list"
$fetch_action = "action=fetch"
$logout_action = "action=logout"
$report_id = $null
# $select_group =  New-Object 'System.Collections.Generic.List[String]' 

# https://qualysguard.qg2.apps.qualys.com/fo/report/report_info.php?id=3763495&view_type=0

# TODO:
# doing this manually for the POC, needs to be fixed.
# $contact_list = @("tsimpson@stanford.edu") 
# $contact_list = @("it.management@law.stanford.edu")
$report_id = "id=$report_id_int"
# $scorecard_id = "id="
$report_list_body = "action=list"

# $report_template_list_php =
# Get base report list
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$report_api/" -Method Post -Body $report_list_body -WebSession $cookie -OutFile "$script_home/reports_list.xml"

### The next three lines will output the header of the response to the Powershell console as a table.
# $response = Invoke-WebRequest -Headers $HttpHeaders -Uri "$QualysPlatform/$report_api/" -Method Post -Body $report_list_body -WebSession $cookie
# Write-Host "The response header is:"
# Write-host ($response.Headers | Format-Table| Out-String)
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $cookie




[System.Xml.XmlDocument]$rl_xml = $(Get-Content -Path .\reports_list.xml)

[System.Windows.Documents.List]$selected_reports = $null

$all_reports_this_round = $rl_xml.REPORT_LIST_OUTPUT.RESPONSE.REPORT_LIST.REPORT

$all_bu_info = Import-Csv $input_file

# [System.Windows.Documents.List]$all_contacts_this_round = @()

$all_contacts_this_round = New-Object 'System.Collections.Generic.List[System.Object]'

# $all_contacts_this_round = @{}



$all_bu_info | ForEach-Object {
    # $bu = $null
    # $rn = $null
    # $list = $null
    $bu = $_.'Business Unit'
    $rn = $_.'Report Title'
    $list = $_.'Exec Summary List'
    if ($rn -like "*Exec*Summary*") {
        #$info = @{}
        # $info = [System.Management.Automation.PSCustomObject]@{reportname = "$rn"; list = "$list";}

        [string]$info = "$rn; $list"
        # $all_contacts_this_round += $info
        $all_contacts_this_round.Add($info)
        # Write-Host "{Name: $rn, list = [$list]}"
        # Write-Host $info
    }
}

$exec_report_matches = New-Object 'System.Collections.Generic.List[System.Object]'

$all_reports_this_round | ForEach-Object {
    $report_id = $_.ID
    $rep_title = $_.TITLE.'#cdata-section'
    $rep_of = $_.OUTPUT_FORMAT
    $rep_status = $_.STATUS.STATE
    $rep_datetime = $_.LAUNCH_DATETIME
    if ($rep_title -like "*Exec*Summary*")
        {
        $exec_report_matches.Add($_)
        # Write-Host "foo: $rep_title, bar: $rep_status, narf: $rep_of, datetime:  $rep_datetime"
        } 

    }

# Write-Host $exec_report_matches        

$full_report_info = New-Object 'System.Collections.Generic.List[System.Object]'


$all_contacts_this_round | ForEach-Object {
    $my_reports = New-Object 'System.Collections.Generic.List[System.Object]'
    $thisrecord = $_
    $report_match = 0
    $thistitle = $thisrecord.Split(';')[0]
    $thislist_base = $thisrecord.Split(';')[1]
    # $thislist = @()
    # $thislist_base.Split(",") | ForEach-Object {$thislist += $($_ -replace '\s','')}
    # $thislist = $thislist_base.ToString() -replace '\s',''

    # Write-Host "Thistitle = $thistitle"
    $exec_report_matches | ForEach-Object {
        $timespan = New-TimeSpan -Days 6
        $now_ish = (Get-Date)
        $launch_dt = [datetime]$_.LAUNCH_DATETIME
        $exec_title = $_.TITLE.'#cdata-section'
        # Write-Host "TITLE: $exec_title"
        # if ($_.TITLE -eq "$thistitle" -and ($now_ish - $launch_dt) -lt $timespan) {
        if ("$exec_title" -eq "$thistitle") {
            $my_reports.Add($_)
            # Write-Host "WHEEEEEEE:  $thistitle"
            $report_match = 1
        }
    }
    if ($report_match -eq 1) {
        $this_id = $($my_reports | Sort-Object -Property LAUNCH_DATETIME| Select-Object -Last 1).ID
        # Write-Host "This_id = $this_id; report_id =  $report_id"
        # Start-Sleep -Seconds 5 -Verbose
        # Write-Host "This_id:  $this_id, input_list:  $thislist_base"
        # Start-Sleep -Seconds 7 -Verbose
        .\report_poc_terse_prod.ps1 -report_id_int $this_id -iso_mode $iso_mode -input_list $thislist_base
    }
}