param (
        [Parameter(Mandatory = $True)]
        [System.Collections.Generic.List[String]]$allfws,
        [Parameter(Mandatory = $False)]
        [System.Collections.Generic.List[String]]$oas_to_split,
        [Parameter(Mandatory = $True)]
        [System.String]$location
        )

$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

# Clear-Host
if ($PSVersionTable.PSVersion.Major -lt 5)
{
        Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
        exit
}
else
{
        # Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}

$xyz = "x"

# $OAS_TO_SPLIT=@("wc-srtr","foa-srtr","foa2-srtr","mc-srtr","nc-srtr","doa-srtr","moa-srtr")

$OAS_TO_SPLIT=@("wc","rwc","sc","mc","nc","dc2","dc1","mn")

# Generate groups of oa-specfic files for upload - contents are IP addresses.
foreach ($fw in $allfws) {
	$oa=$fw.Replace("-srtr","")
	$master_filename=$xyz+"-"+$oa+".MASTER.txt"
	# Write-Debug "fw:  $fw"
	# Get count of lines
	# $countpath = $("$script_home/firewall/$filehash/upload/"+$master_filename)
    $countpath = $("$location/current_netfiles/$master_filename")
	# Write-Host "Countpath:  $countpath"
	$countlines=$(Get-Content -path $countpath|Measure-Object -Line).Lines
	# Write-Debug "OA $oa has $countlines for whee"
	if ($OAS_TO_SPLIT -contains $oa ) {
	    $i = $null
	    $j = 1
	    $total = 0
	    # Write-Debug "countlines:  $countlines"
	    $num_groups = 10
	    $groupsize = $countlines/$num_groups
	    $groupsize1 = $groupsize.ToString().Split('.')[0]
	    $groupsize = [int]$groupsize1
	    $last_group = $countlines%$num_groups

	    $max_count = $countlines

	    $max_per_pass = [int]$($max_count / $num_groups)
	    # Write-Debug "Max_per_pass: $max_per_pass"
	    # Start-Sleep -s 2
	    $checklines=$(Get-Content -Path $countpath)

	    # checklines > "$script_home/firewall/$filehash/upload/$master_filename"
        $checklines > "$location/current_netfiles/$master_filename"

	    $list = New-Object 'System.Collections.Generic.List[String]'
	    ForEach ($row in $checklines) {
		$list.Add($row)
		$i++
		$total++
		if ($i -eq $max_per_pass -AND $j -lt $num_groups) {
			# $writepath = "$script_home/firewall/$filehash/upload/"+$xyz+"-"+$oa+"-group-"+$j+".txt"
            $writepath = "$location/current_netfiles/"+$xyz+"-"+$oa+"-group-"+$j+".txt"
            Out-File -FilePath $writepath -encoding ascii -inputobject $list
			$list = New-Object 'System.Collections.Generic.List[String]'
			$i = $null
			$j++
		} elseif ($j -eq $num_groups -AND $total -ge $max_count) {
			# Write-Debug "total:$total"
			# Write-Debug "j:$j"
			# Write-Debug "row:$row"
			$writepath = "$location/current_netfiles/"+$xyz+"-"+$oa+"-group-"+$j+".txt"
			# $writepath = "$script_home/firewall/$filehash/upload/"+$xyz+"-"+$fw+"-group-"+$j+".txt"
			Out-File -FilePath $writepath -encoding ascii -inputobject $list
			}
		}
    } 
}

# return $location