param (
        [Parameter(Mandatory = $False)]
        [System.String]$filepath = "D:\git\qualys_ag_auto\net2oa.txt"
)

$script_home = "D:\git\qualys_ag_auto"

# Clear-Host
if ($PSVersionTable.PSVersion.Major -lt 5)
{
        Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
        exit
}
else
{
        # Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}


# Source this local .ps1 file.  This allows for easy expansion of ranges received from Qualys.
. "$script_home/Get-IPrange.ps1"

##### The special vsys lists (numbers are vsys numbers)

# FOA2-srtr: BACKUP-SRV (vsys32)
$foa2_vsys=@(32)
# FOA-srtr:  ASIA-PROD (vsys80), STG-MGM (vsys4), PSOFT-UAT (vsys55), BIGFIX (vsys5)
# $foa_vsys=@(80,4,55,5)
$foa_vsys=@(4,55,5)

function just_all_fws
{
        param
        (
                [Parameter(Mandatory = $true)]
                $filename,
                [Parameter(Mandatory = $true)]
                $fwdir
        )
    # Write-Host 
    $file = Get-Content -Path $filepath
    $drop_header = $file[1..($file.Count-1)]
    # Remove-Item "$script_home/firewall/content.csv" -Force
    # $drop_header | Select-String -Pattern "1.1.1.0/26" -NotMatch > "$script_home/firewall/content.csv"
    $drop_header > "$script_home/firewall/content.csv"
    $header = "Address Space", "First IP", "LastIP", "OA", "Vlan", "Firewall", "Interface", "Vsys", "zone", "Buildings"
    $content = Import-Csv "$script_home/firewall/content.csv" -Header $header -Delimiter "|"
    # Remove-Item "$script_home/firewall/content.csv" -Force

    $content | ForEach-Object {
        $fw=$_.Firewall
        $oa_list.Add($fw)
    }
    $all_fw = New-Object 'System.Collections.Generic.List[String]'
    # $oa_list was assembled in the loop above
    $oa_list | Sort -Unique | ForEach-Object {$all_fws.Add($_)}
    $location = "$script_home\firewall\$filehash"
    return $all_fw
    # return @($all_fws,$location)
}

function process_file {
        param
        (
                [Parameter(Mandatory = $true)]
                $filename,
                [Parameter(Mandatory = $true)]
                $fwdir
        )
    $file = Get-Content -Path $filename
    $drop_header = $file[1..($file.Count-1)]
    Remove-Item "$script_home/firewall/content.csv" -Force
    #$drop_header | Select-String -Pattern "1.1.1.0/26" -NotMatch > "$script_home/firewall/content.csv"
    $drop_header > "$script_home/firewall/content.csv"
    $header = "Address Space", "First IP", "LastIP", "OA", "Vlan", "Firewall", "Interface", "Vsys", "zone", "Buildings"
    $fwcontent = Import-Csv "$script_home/firewall/content.csv" -Header $header -Delimiter "|"
    # Remove-Item "$script_home/firewall/content.csv" -Force

    

#     $fwcontent| ForEach-Object {
#        $all_address_spaces.Add($_.'Address Space')
#    }

    $fwcontent | ForEach-Object {
        $fw=$_.Firewall
        # Write-Host "FW is $fw"
        $vsys=$_.Vsys.ToInt32($null)
        $addr=$_.'Address Space'
        $filename_fw="nonspecified"
        # 
        $oa = $null
        if ($fw -match "srtr") {
            $oa_list.Add($fw)
            $oa = $fw.Replace("-srtr","")
        } else {
            $oa = "nonspecified"
        }
        $cidr_list = New-Object 'System.Collections.Generic.List[String]'
         
        # Start-Sleep -s 2
        # Write-Host "#--- fw:  $fw, vsys:  $vsys, addr:  $addr, oa: $oa"
        # if ($oa) {
        # ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $vsys -eq '4') {
        #     $filename_fw="current_netfiles/"+$xyz+"-stg-mgm-vsys4.txt"
        #     (Invoke-PSipcalc -NetworkAddress $addr -Enumerate).IPEnumerated|ForEach-Object {$cidr_list.Add($_)}
        #     $address=$cidr_list
        #     # Write-Host "Addr:$addr, $vsys"
        # } elseif ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $vsys -eq '55') {
        #     $filename_fw="current_netfiles/"+$xyz+"-psoft-vsys55.txt"
        #     (Invoke-PSipcalc -NetworkAddress $addr -Enumerate).IPEnumerated|ForEach-Object {$cidr_list.Add($_)}
        #     $address=$cidr_list
        #     # Write-Host "Addr:$addr, $vsys"
        # } elseif ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $vsys -eq '5') {
        #     $filename_fw="current_netfiles/"+$xyz+"-bigfix-vsys5.txt"
        #     (Invoke-PSipcalc -NetworkAddress $addr -Enumerate).IPEnumerated|ForEach-Object {$cidr_list.Add($_)}
        #     $address=$cidr_list
        #     # Write-Host "Addr:$addr, $vsys"
        # } elseif ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $vsys -eq '80') {
        #     $filename_fw="current_netfiles/"+$xyz+"-asprod-vsys80.txt"
        #     (Invoke-PSipcalc -NetworkAddress $addr -Enumerate).IPEnumerated|ForEach-Object {$cidr_list.Add($_)}
        #     $address=$cidr_list
        #     # Write-Host "Addr:$addr, $vsys"
        # } elseif ($fw -like "foa2*" -and $foa2_vsys.Contains($vsys)) {
        #     $filename_fw="current_netfiles/"+$xyz+"-special-"+$oa+".txt"
        #     (Invoke-PSipcalc -NetworkAddress $addr -Enumerate).IPEnumerated|ForEach-Object {$cidr_list.Add($_)}
        #     $address=$cidr_list
        #     # Write-Host "Addr:$addr, $vsys"
#       #   } elseif ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -AND $vsys -ne '80' -AND $vsys -ne '4' -AND $vsys -ne '5' -AND $vsys -ne '55'){
#       #      $filename_fw="current_netfiles/"+$xyz+"-special-"+$oa+".txt"
#	    #     $address=$addr
        #     # Write-Host "Special but not FOA2 Addr:$addr, $vsys"
        # } else {
        $filename_fw="current_netfiles/"+$xyz+"-"+$oa+".MASTER.txt"
        # Write-Debug "cOutput:$fw, $vsys"
        # Write-Host "Everything else: $addr, $vsys, $oa"
        (Invoke-PSipcalc -NetworkAddress $addr -Enumerate).IPEnumerated|ForEach-Object {$cidr_list.Add($_)}
        $address=$cidr_list
      $address | Out-File -NoClobber -Append -FilePath "$script_home/firewall/$filehash/$filename_fw" -Encoding ascii
      }
    $all_fw = New-Object 'System.Collections.Generic.List[String]'
    # $oa_list was assembled in the loop above
    $oa_list | Sort -Unique | ForEach-Object {$all_fw.Add($_)}
    # $location = "$script_home\firewall\$filehash"
    

    $all_fw > "$fwdir\all_fw.txt"
    # return @($all_fw,$location)
    }


####////////////////////////////

$xyz = "x"
$xyz_search = "x-*"
$summary_list = New-Object 'System.Collections.Generic.List[String]'
$oa_list = New-Object 'System.Collections.Generic.List[String]'
$all_address_spaces = New-Object 'System.Collections.Generic.List[String]'

# The path to save the file retrieved from the networking team AFS.
# $netfile = "$script_home/net2oa.txt"
$file = Get-Content -Path $filepath

# Make a hash unique to this file and make a subdirectory based on that hash
$filehash = (Get-FileHash -Path $filepath -Algorithm MD5).Hash
$fwdir = "$script_home\firewall\$filehash" 
$fwdir > "$script_home\firewall\fwdir.txt"
$date = $(Get-Date -UFormat "%Y%m%d%H%M%S")
$all_fw = $null
$location = $null
if (!(Test-Path -Path $fwdir/current_netfiles)) {
    New-Item -Path $fwdir -ItemType directory
    $filename = "$fwdir\$date.csv"
    Copy-Item -Path $filepath -Destination $filename -Force
    New-Item "$fwdir\current_netfiles" -ItemType directory
    # $location = $fwdir
    process_file -filename $filename -fwdir $fwdir
    # return $all_fws
    #  $all_fw > "$fwdir\all_fw.txt"
    # Write-Host "Duh:  $all_fw"
    # return @($all_fw,$location)

    # .\Build-X-Subgroups.ps1 -all_fws $all_fws -location $fwdir

    } else {
        $filename = "$fwdir\$date.csv"
        Copy-Item -Path $filepath -Destination $filename -Force
        # $all_fw = $null
        process_file -filename $filename -fwdir $fwdir
        # return $all_fws
        # $location = $fwdir
        # Write-Host "elseLocation:  $location"
        # Write-Host "Buh:  $all_fw"
        # $all_fw > "$fwdir\all_fw.txt"
        # return @($all_fw,$location)
    }



# $all_fws = New-Object 'System.Collections.Generic.List[String]'
# $oa_list was assembled in the loop above
# $oa_list | Sort -Unique | ForEach-Object {$all_fws.Add($_)}
# $location = "$script_home\firewall\$filehash"
# Write-Host "Location:  $location"
# return @($all_fw,$location)
# return $all_fws
# return $location