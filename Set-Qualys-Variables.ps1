param (
        [Parameter(Mandatory = $True)]
        [System.String]$QualysUsername,
        [Parameter(Mandatory = $True)]
        [System.String]$QualysPassword
)

# $DebugPreference = "Continue"
# $QualysUserName = $(get-Content -Path "$script_home/qualysuser.txt")
# $QualysPassword = $(get-Content -Path "$script_home/qualyspw.txt")

$DebugPreference = "Continue"

# Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser
# Clear-Host
if ($PSVersionTable.PSVersion.Major -lt 5)
{
        Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
        exit
}
else
{
        # Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}



function set_qualys_variables
{
        param
        (
                [Parameter(Mandatory = $true)]
                [System.String]$QualysUsername,
                [Parameter(Mandatory = $true)]
                [System.String]$QualysPassword
        )
	$QualysPlatform = "https://qualysapi.qg2.apps.qualys.com/api/2.0/fo"

	$password_base64 = ConvertTo-SecureString $QualysPassword -AsPlainText -Force  
	$creds = New-Object System.Management.Automation.PSCredential ($QualysUsername, $password_base64) 

	$BasicAuthFormedCredential = "Basic $creds"
	$HttpHeaders = @{'Authorization' = $BasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}
	$login_action = "action=login&username=$QualysUsername&password=$QualysPassword&echo_request=0"
	$logout_action = "action=logout"


    # return $QualysPlatform,$HttpHeaders,$creds,$login_action,$QualysUsername,$QualysPassword
    return $QualysPlatform,$HttpHeaders,$creds,$login_action,$logout_action
}


set_qualys_variables -QualysUsername $QualysUsername -QualysPassword $QualysPassword



