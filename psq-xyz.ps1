$DebugPreference = "Continue"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Set $script_home to the script's parent's path
$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$mydate_main = $(Get-Date -Format s)

Write-Debug "Script home = $script_home"

# Source this local .ps1 file.  This allows for easy range expansion of ranges received from Qualys.
. "$script_home/Get-IPrange.ps1"

##### The special vsys lists (numbers are vsys numbers)
# FOA2-srtr: BACKUP-SRV (vsys32)
$foa2_vsys=@(32)
# FOA-srtr:  ASIA-PROD (vsys80), STG-MGM (vsys4), PSOFT-UAT (vsys55), BIGFIX (vsys5)
# $foa_vsys=@(80,4,55,5)
$foa_vsys=@(4,55,5)

$summary_list = New-Object 'System.Collections.Generic.List[String]'
$oa_list = New-Object 'System.Collections.Generic.List[String]'


# The path to save the file retrieved from the networking team AFS.
$netfile = "$script_home/net2oa.txt"
$file = Get-Content -Path $netfile


# Make a hash unique to this file and make a subdirectory based on that hash
$filehash = (Get-FileHash -Path $netfile -Algorithm MD5).Hash
$fwdir = "$script_home/firewall/$filehash" 

$xyz = "x"
$xyz_search = "x-*"


Remove-Item "$script_home/firewall/$filehash" -Force -Recurse
New-Item "$script_home/firewall/$filehash" -ItemType directory -Force
New-Item "$script_home/firewall/$filehash/upload" -ItemType directory

$drop_header = $file[1..($file.Count-1)]
Remove-Item "$script_home/firewall/content.csv" -Force
#$drop_header | Select-String -Pattern "1.1.1.0/26" -NotMatch > "$script_home/firewall/content.csv"
$drop_header > "$script_home/firewall/content.csv"
$header = "Address Space", "First IP", "LastIP", "OA", "Vlan", "Firewall", "Interface", "Vsys", "zone", "Buildings"
$content = Import-Csv "$script_home/firewall/content.csv" -Header $header -Delimiter "|"
#$content = Import-Csv "$script_home/net2oa.csv"

  # Send CIDR block to a file named firewall/$filehash/<firewall_name>_<vsys>.txt
  # Note we're only enumerating every IP in the range for CIDR blocks which 
  # don't match any of the special vsys, because those don't need to be later subdivided
  # by number of IP addresses.
  $content | ForEach-Object {
    $fw=$_.Firewall
    $vsysstring=$_.Vsys
    $vsys=[convert]::ToInt32($vsysstring)
    $addr=$_.'Address Space'
    $filename="null"
    $oa_list.Add($fw)
    $cidr_list = New-Object 'System.Collections.Generic.List[String]'
    # Start-Sleep -s 2
    # Write-Debug "######## fw:  $fw, Avsys_inside:  $vsys"
    if ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -AND $vsys -ne '80' -AND $vsys -ne '4' -AND $vsys -ne '5' -AND $vsys -ne '55' -AND $foa_vsys.Contains($vsys))
    {
        # Write-Debug "AOutput:$fw, $vsys"
        $filename="special"+$fw+"-"+$vsys+".txt"
        $filename_fw="upload/"+$xyz+"-special-"+$fw+".txt"
	$address=$addr
        # Write-Debug "filename:  $filename"
#     } elseif ($fw -like "foa2" -AND -and $vsys -eq '32') {
#         # $filename_fw="upload/"+$xyz+"-special-"+$fw+".txt"
#         $filename_fw="upload/"+$xyz+"-stg-mgm-vsys32.txt"
# 	$address=$addr
        # Write-Debug "BOutput:$fw, $vsys"
    } elseif ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $vsys -eq '4') {
        # $filename_fw="upload/"+$xyz+"-special-"+$fw+".txt"
        $filename_fw="upload/"+$xyz+"-stg-mgm-vsys4.txt"
	$address=$addr
        Write-Debug "BOutput:$fw, $vsys"
    } elseif ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $vsys -eq '55') {
        # $filename_fw="upload/"+$xyz+"-special-"+$fw+".txt"
        $filename_fw="upload/"+$xyz+"-psoft-vsys55.txt"
	$address=$addr
        Write-Debug "BOutput:$fw, $vsys"
    } elseif ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $vsys -eq '5') {
        # $filename_fw="upload/"+$xyz+"-special-"+$fw+".txt"
        $filename_fw="upload/"+$xyz+"-bigfix-vsys5.txt"
	$address=$addr
        Write-Debug "BOutput:$fw, $vsys"
    } elseif ($fw -like "foa*" -AND $fw -NOTlike "foa2*" -and $vsys -eq '80') {
        # $filename_fw="upload/"+$xyz+"-special-"+$fw+".txt"
        $filename_fw="upload/"+$xyz+"-asprod-vsys80.txt"
	$address=$addr
        # Write-Debug "BOutput:$fw, $vsys"
    } elseif ($fw -like "foa2*" -and $foa2_vsys.Contains($vsys)) {
        # Write-Debug "bOutput:$fw, $vsys"
        $filename="special"+$fw+"-"+$vsys+".txt"
        $filename_fw="upload/"+$xyz+"-special-"+$fw+".txt"
	$address=$addr
        # Write-Debug "BOutput:$fw, $vsys"
    } else {
	# $addresses= Get-IPrange -start $start -end $end 
	(Invoke-PSipcalc -NetworkAddress $addr -Enumerate).IPEnumerated|ForEach-Object {$cidr_list.Add($_)}
        $filename=$fw+"-"+$vsys+".txt"
        $filename_fw="upload/"+$xyz+"-"+$fw+".MASTER.txt"
	$address=$cidr_list
        # Write-Debug "cOutput:$fw, $vsys"
    }
    $address | Out-File -NoClobber -Append -FilePath "$script_home/firewall/$filehash/$filename_fw" -Encoding ascii
  }


$all_fws = New-Object 'System.Collections.Generic.List[String]'
# $oa_list was assembled in the loop above
$oa_list | sort | Get-Unique | ForEach-Object {$all_fws.Add($_)}

# Start-Sleep -s 2
# Write-Debug "All_fws variable:  $all_fws"

# These oa strings will be split into ten [x|y] groups for scan sizing.
$OAS_TO_SPLIT=@("wc-srtr","foa-srtr","foa2-srtr","mc-srtr","nc-srtr","doa-srtr","moa-srtr")

# Generate groups of oa-specfic files for upload - contents are IP addresses.
foreach ($fw in $all_fws) {
	$oa=$fw.Replace("-srtr","")
	$master_filename=$xyz+"-"+$fw+".MASTER.txt"
	# Write-Debug "fw:  $fw"
	# Get count of lines
	$countpath = $("$script_home/firewall/$filehash/upload/"+$master_filename)
	# Write-Debug "Countpath:  $countpath"
	$countlines=$(Get-Content -path $countpath|Measure-Object -Line).Lines
	# Write-Debug "OA $oa has $countlines for whee"
	if ($OAS_TO_SPLIT -contains $fw ) {
	    $i = $null
	    $j = 1
	    $total = 0
	    # Write-Debug "countlines:  $countlines"
	    $num_groups = 10
	    $groupsize = $countlines/$num_groups
	    $groupsize1 = $groupsize.ToString().Split('.')[0]
	    $groupsize = [int]$groupsize1
	    $last_group = $countlines%$num_groups

	    $max_count = $countlines
	    $max_per_pass = [int]$($max_count / $num_groups)
	    # Write-Debug "Max_per_pass: $max_per_pass"
	    # Start-Sleep -s 2
	    $checklines=$(Get-Content -Path $countpath)

	    $checklines > "$script_home/firewall/$filehash/upload/$master_filename"
	    $list = New-Object 'System.Collections.Generic.List[String]'
	    ForEach ($row in $checklines) {
		$list.Add($row)
		$i++
		$total++
		if ($i -eq $max_per_pass -AND $j -lt $num_groups) {
			$writepath = "$script_home/firewall/$filehash/upload/"+$xyz+"-"+$oa+"-group-"+$j+".txt"
			Out-File -FilePath $writepath -encoding ascii -inputobject $list
			$list = New-Object 'System.Collections.Generic.List[String]'
			$i = $null
			$j++
		} elseif ($j -eq $num_groups -AND $total -ge $max_count) {
			# Write-Debug "total:$total"
			# Write-Debug "j:$j"
			# Write-Debug "row:$row"
			$writepath = "$script_home/firewall/$filehash/upload/"+$xyz+"-"+$oa+"-group-"+$j+".txt"
			# $writepath = "$script_home/firewall/$filehash/upload/"+$xyz+"-"+$fw+"-group-"+$j+".txt"
			Out-File -FilePath $writepath -encoding ascii -inputobject $list
			}
		}
	    } #else {
	        # $checklines=$(Get-Content -Path $countpath)
		# $checklines > "$script_home/firewall/$filehash/upload/$master_filename"
		
		# Write-Debug "Writepath:$writepath"
		# Write-Debug "oa:$oa"
		# Write-Debug "fw:$fw"
		# Out-File -FilePath $writepath -encoding ascii -inputobject $checklines
	#	   }

	}


function b64encodepwd
{
	[CmdletBinding(SupportsShouldProcess = $false)]
	[OutputType('System.String')]
	param
	(
		[Parameter(Mandatory = $true)]
		[System.String]$id,
		[Parameter(Mandatory = $true)]
		[System.String]$pwd
	)
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    $EncodedPwd = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $id, $pwd)))
    Remove-Variable id
    Remove-Variable pwd
	#[System.String]$EncodedPwd = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes("$($id):$($pwd)"))
	if ($debugmsgs)
	{
		write-warning "DERBERG:  $debugmsgs"
	}
	return $EncodedPwd
}

$QCreds = Import-Clixml -Path "$script_home/sunet2ma.creds"


# Get a qualys session, get a list of asset groups, destroy the session
# Set up qualys API login info.   NOTE these first two lines will be different per user/group.
# $QualysUserName = $(get-Content -Path "$script_home/qualysuser.txt")
# $QualysPassword = $(get-Content -Path "$script_home/qualyspw.txt")

$QualysUserName = $QCreds.UserName
$QualysPassword = $QCreds.Password

$b64 = b64encodepwd -id $QualysUsername -pwd $QualysPassword

$QualysPlatform = 'https://qualysapi.qg2.apps.qualys.com/api/2.0/fo'
# $BasicAuthString = [System.Text.Encoding]::UTF8.GetBytes("$QualysUserName`:$QualysPassword")

# $BasicAuthBase64Encoded = [System.Convert]::ToBase64String($BasicAuthString)
<<<<<<< HEAD
# $password_base64 = [System.Convert]::ToBase64String($QualysPassword)
$password_base64 = ConvertTo-SecureString $QualysPassword -AsPlainText -Force  
$creds = New-Object System.Management.Automation.PSCredential ($QualysUserName, $password_base64) 
=======
#$password_base64 = ConvertTo-SecureString $QualysPassword -AsPlainText -Force  
# $creds = New-Object System.Management.Automation.PSCredential ($QualysUserName, $password_base64) 
>>>>>>> remotes/origin/master

$BasicAuthFormedCredential = "Basic $b64"
$HttpHeaders = @{'Authorization' = $BasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}


# API actions and endpoints
$login_action = "action=login&username=$QualysUserName&password=$QualysPassword"
# $login_action = "action=login"
$logout_action = "action=logout"

$report_api = "report"
$all_reports_query = "action=list&output_format=csv"

$asset_group_api = "asset/group"
# $all_asset_titles_id = "action=list&show_attributes=TITLE&output_format=csv"

$all_asset_titles_id = "action=list&show_attributes=TITLE&output_format=csv"
$select_group =  New-Object 'System.Collections.Generic.List[String]' 

##### Available data fields for asset groups
### ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
### HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME

# Log in to Qualys.
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Credential $creds -Body $login_action -SessionVariable sess
# Start-Sleep -s 2

# Get Asset Group list into a csv file.
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $all_asset_titles_id -WebSession $sess -OutFile "$script_home/firewall/check_ag.csv"
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$report_api/" -Method Post -Body $all_reports_query -WebSession $sess -OutFile "$script_home/firewall/check_reports.csv"



$check_csv_full = $(Get-Content -Path "$script_home/firewall/check_ag.csv")
$my_xyz_groups =  New-Object 'System.Collections.Generic.List[String]' 
$my_xyz_objects = New-Object 'System.Collections.ArrayList'
$my_xyz_ids = New-Object 'System.Collections.Generic.List[String]' 

# First output line is the indicator of success!
$lineone = $check_csv_full[0]
Write-Debug "check_csv_full[0]: $lineone"
if ($lineone -like "----BEGIN_RESPONSE_BODY_CSV") 
   { 
   # This approach includes the header line, which is usually line 2 of the response at least for asset group output
   $check_csv_full[1..($check_csv_full.Count-2)] > "$script_home/firewall/response_content.csv"
   # $response_content becomes the reference point for all AG > ID lookups
   $response_content = Import-Csv "$script_home/firewall/response_content.csv" 
   
   $response_content | ForEach-Object {
			$id=$_.ID
			$title=$_.TITLE
			Write-Debug "my_groups:  $title,$id" 
            if ($title -like $xyz_search) {
    		        $my_xyz_groups.Add("$title")
	    	        $my_xyz_ids.Add("$id")
		            $my_xyz_objects.Add("$_")
                }
			}
  # Get the extended data just for the Asset Groups we care about
  $my_xyzgroups = $my_xyz_groups -join ","
  $my_xyzids = $my_xyz_ids -join ","


  ################
  ###-----> Valid attributes for request of Asset groups in Qualys v2 api:  
  ###
  # ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
  # HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME
  #
  ####################################################

  $selected_xyz_asset_titles_body = "action=list&ids=$my_xyzids&show_attributes=ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS&output_format=csv"


  # Write-Debug "#### CURRENT:  my_y_groups:  $my_y_groups"
  # Write-Debug "Get info for selected asset groups"
  Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $selected_xyz_asset_titles_body -WebSession $sess -OutFile "$script_home/firewall/selected_xyz_check_ag.csv"
  
  } 



# Get the current xyz-asset group info from csv
$current_xyz_group_info = $(Get-Content -Path "$script_home/firewall/selected_xyz_check_ag.csv")
# Get the header
$current_xyz_headervals = $current_xyz_group_info[1]
$current_xyz_headers = $current_xyz_headervals.Split(",").Replace("`"","") 

# Add columns to the header for overloading
# $add = '"PROPOSED_IP_SET","DELTA","DIFF_FOR_COMMENTS"'


Write-Debug "#### CURRENT:  current_xyz_headers:  $current_xyz_headers"


# Get just the content
$current_xyz_group_info[2..($current_xyz_group_info.Count-2)] > "$script_home/firewall/current_xyz_group_info.csv"
$current_xyz_group_content = Import-Csv "$script_home/firewall/current_xyz_group_info.csv" -Header $current_xyz_headers -Delimiter ","


# Get list of upload files
$files = $(Get-ChildItem -Force "$script_home/firewall/$filehash/upload" -File) 

# for each of the files
$files | ForEach-Object {
	# get asset group name
    $name = $_.Name
	$agname = $_.Name.TrimEnd(".txt")
	if ($agname -contains "MASTER") {
		Write-Debug "AG_name1: ----------------------------------------- $agname contains MASTER"
        	# $agname = $agname.TrimEnd("-srtr.MASTER")
		$agname=$agname.Replace("-srtr.MASTER","")
	   } else { 
		# $agname = $agname.TrimEnd("-srtr")
		$agname=$agname.Replace("-srtr","")
		Write-Debug "AG_name2: ----------------------------------------- [$agname]"
	}
	Write-Debug "AG_name outside: ----------------------------------------- [$agname]"
	# and proposed IP addresses
        # $content = Get-Content -Path "$script_home/firewall/$filehash/upload/$name" 
        # Write-Debug "AG_name: $agname"
        # $ips = $($content -join ",")

	# $new_ips_expanded = New-Object 'System.Collections.Generic.List[String]'
	$new_ips_expanded = Get-Content -Path "$script_home/firewall/$filehash/upload/$name" 

	# Assign the values (CIDR blocks) from the individual network files to the new_ips_expanded list as expanded ranges
	# $content| ForEach-Object {(Invoke-PSipcalc -NetworkAddress $_ -Enumerate).IPEnumerated|ForEach-Object {$new_ips_expanded.Add($_)}}
	
	$ips_count=$new_ips_expanded.Count

	$ips=$new_ips_expanded -join ","
	$agid = $null
	
	# Check whether AG already exists
	# $mx_xyz_groups is a global and a cheat, I'm sorry with a Canadian accent.
	if ($my_xyz_groups -contains $agname) {
		# If AG already exists, grab its ID
		# It's an edit operation!  We need an asset group ID for this to work.
		# Write-Debug "my_xyz_objects = $my_xyz_objects"
		$matching_ag_id = $null
		$old_ips = $null
		$comments = $null
		$existing_ips_expanded = New-Object 'System.Collections.Generic.List[String]'
		# $new_ips| ForEach-Object {(Invoke-PSipcalc -NetworkAddress $_ -Enumerate).IPEnumerated}

		# This will be the whole object which matches the AG name
		$chosen = $($current_xyz_group_content| Where-Object {$_.TITLE -eq "$agname"})
		$matching_ag_id = $($chosen.ID)
		$agid = $($chosen.ID)
		
		if ($matching_ag_id -ne $null) {
			$old_ips=$($chosen.IP_SET)
			$old_ips.Split(',') | ForEach-Object {
			        # Write-Debug "POSITION ONE:  $_"
				if ($_.contains('-')) {
				   # It's a range
				   $start=$_.Split("-")[0].ToString()
				   $end=$_.Split("-")[1].ToString()
				   # Write-Debug "POSITION ALPHA start: $start; end: $end"
				   $rc = Get-IPrange -start $start -end $end 
				   # Write-Debug "POSITION WTF rc: $rc"
				   # $existing_ips_expanded.Add($rc)
				   $rc | ForEach-Object {
				        $existing_ips_expanded.Add($_)
					# Write-Debug "Position two: $_"
				   }
				   #$list.Add($row)
				} else {
				   # Write-Debug "POSITION BETA: $_"
				   $existing_ips_expanded.Add($_)
				}

			}

		        # $existing_ips_expanded.Add($_)

			# $old_ips_expanded = $old_ips
			# $old_comments=$($chosen.COMMENTS)
			# Write-Debug "Not null!: $($chosen.ID)"
			Write-Debug "###################### UPGRAAAAADE! Name $agname found!!! Existing AG $agname will need to be updated. ID is $matching_ag_id"
			# Write-Debug "IP Set: $old_ips to be written to comments field"
			#Write-Debug "NEW IP Set: $ips"
			# Write-Debug "old_comments:  $old_comments"

			# Using Get-Date for now since the comments field is too small for diffs
			$comments = "Updated:  $(Get-Date -Format s)"
			# if ($ips_and_comments_same -eq True) {
# asdf
			#	
			#}
		}
		# $check_old_vs_new = ?($new_ips_expanded -notcontains $existing_ips_expanded)

		# if there is something to remove, or something in new_vs old, do stuff

		# https://stackoverflow.com/questions/19012457/compare-two-list-and-find-names-that-are-in-list-one-and-not-list-two-using-powe
		$to_remove=$($existing_ips_expanded | ?{$new_ips_expanded -notcontains $_})
		# $comments=$to_remove -join ","
		$check_new_vs_old=$($new_ips_expanded |?{$existing_ips_expanded -notcontains $_})
		$check_overlap=$($new_ips_expanded | ?{$existing_ips_expanded -contains $_})
		# $FolderList | ?{$AdUserName -notcontains $_}

		# 
	
		# Write-Debug "----------- existing_ips_expanded:  $existing_ips_expanded"
		# Write-Debug "----------- new_ips_expanded:  $new_ips_expanded"
		$to_remove_Count=$to_remove.Count
		$check_new_vs_old_Count=$check_new_vs_old.Count
		$check_overlap_Count=$check_overlap.Count
		# Write-Debug "----------- to_remove:  $to_remove"
		if ($to_remove_Count -gt 0 -Or $check_new_vs_old_Count -gt 0 -And $ips_count -gt 0) {
			# Write-Debug "----------- to_remove: $to_remove_Count"
			# Write-Debug "----------- check_new_vs_old: $check_new_vs_old_Count"
			# Write-Debug "----------- total new ips: $ips_count"
			# Write-Debug "----------- check_overlap_Count:  $check_overlap_Count"
			$action_asset_group_body = "action=edit&id=$matching_ag_id&set_ips=$ips&set_comments=$comments"
			# $action_ag_post_check = "action=edit&id=$matching_ag_id&set_ips=$ips&set_comments=$comments"
		} else {
		$action_asset_group_body = "action=list&ids=$matching_ag_id"
		Write-Debug "===== continue - no change section: should only hit here if new, remove, and payload are all 0"
		#continue
		# break
		}
		# Write-Debug "----------- check_overlap: $check_overlap_Count"
		# Write-Debug "action=edit&id=$matching_ag_id&set_ips=$ips&set_comments=$comments"
		} else {
		# Should be a new asset group name
		Write-Debug "Name $agname will be a new AG."
		$action_asset_group_body = "action=add&title=$agname&ips=$ips"
 		}

	$action_aftercheck = "action=list&ids=$agid&show_attributes=ID,TITLE,IP_SET&output_format=csv"

	Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $sess
	# Start-Sleep -s 2
	Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Credential $creds -Body $login_action -SessionVariable sess

	###### uncomment to actually upload / change asset groups
	# The actual qualys call to add/update the AG
	Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $action_asset_group_body -WebSession $sess -OutFile "$script_home/firewall/check_asset_group_add.txt"

	Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $sess
	# Start-Sleep -s 2
	Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Credential $creds -Body $login_action -SessionVariable sess


  	Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $action_aftercheck -WebSession $sess -OutFile "$script_home/firewall/aftercheck_y.csv"

	$post_info = $(Get-Content -Path "$script_home/firewall/aftercheck_y.csv")
	$post_info[2..($post_info.Count-2)] > "$script_home/firewall/aftercheck_xyz_info.csv"
	$post_headervals = $post_info[1]
	$post_xyz_headers = $post_headervals.Split(",").Replace("`"","")
	$post_xyzy_content = Import-Csv "$script_home/firewall/aftercheck_xyz_info.csv" -Header $post_xyz_headers -Delimiter ","
	$post_action_aglist = New-Object 'System.Collections.Generic.List[String]'
	$rangeset = New-Object 'System.Collections.Generic.List[String]'
	$ipset = New-Object 'System.Collections.Generic.List[String]'

	# add expanded output.Count from post-check
	$post_xyz_content | ForEach-Object {
		# $ipset=$_.IP_SET.ToString()
		# $ipset=$_.IP_SET.Split(',')
		$ipset=$_.IP_SET
		# Write-Debug "POSITION ONE:  $ipset.split(',')"
		if ($ipset -notcontains(',')) {
			# Write-Debug "POSITION FUUUUU:  $ipset"
			$rangeset.Add($ipset)
			} else {
			$ipset.Split(',') | ForEach-Object {
			# Write-Debug "POSITION FUUUUU:  $ipset"
			$rangeset.Add($_)
			}
		}
		# Write-Debug "POSITION ONE:  $ipset"
		# Write-Debug "POSITION ONE:  "+$ipset.split(',')
		$rangeset| ForEach-Object {
			if ($_.contains('-')) {
			   $start=$_.Split("-")[0].ToString()
			   $end=$_.Split("-")[1].ToString()
			   # Write-Debug "POSITION ALPHA start: $start; end: $end"
			   $rc = Get-IPrange -start $start -end $end
			   # Write-Debug "POSITION WTF rc: $rc"
			   # $existing_ips_expanded.Add($rc)
			   $rc | ForEach-Object {
				$post_action_aglist.Add($_)
				# Write-Debug "Position two: $_"
				}
			} else {
		   # Write-Debug "POSITION BETA: $_"
		   # $existing_ips_expanded.Add($_.IP_SET)
		   $post_action_aglist.Add($_.IP_SET)
		}

	}
	$tmp_count=$post_action_aglist.Count
        # Write-Debug "post_y_content $agname, ip count: $tmp_count"
	# Write-Debug "#######----------- to_remove: $to_remove_Count"
	$this_round = @($agname,$tmp_count,$to_remove_Count,$check_new_vs_old_Count,$check_overlap_Count)
	$summary_list.Add("$this_round")
	#$HttpHeaders = @{'Authorization' = $BasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}
	# Write-Debug "Summary list: $summary_list"
	# $summary_list >> "$script_home/firewall/summary_$mydate_main.csv" 
	# $this_round -join "," >> "$script_home/firewall/summary_$mydate_main.csv" 
	}.
}

# Log out of Qualys.
Write-Debug "Logging out of Qualys"
# Write-Debug "logout_action:  $logout_action"
# Write-Debug "sess:  $sess"

$mydate = $(Get-Date -Format s)

# asdf
$summary_list | Export-Csv -Path "$script_home/firewall/summary_$mydate.csv" 

# Get the post-upload data for changed asset groups

Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $sess

# . "$script_home/Get-Qualys-Users.ps1"
# Get-Qualys-Users

