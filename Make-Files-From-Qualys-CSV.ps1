param (
        [Parameter(Mandatory = $True)]
        [System.String]$input_file,
        [Parameter(Mandatory = $True)]
        [System.String]$file_destination
)

# $DebugPreference = "Continue"
# $QualysUserName = $(get-Content -Path "$script_home/qualysuser.txt")
# $QualysPassword = $(get-Content -Path "$script_home/qualyspw.txt")
. .\Get-IPrange.ps1

$DebugPreference = "Continue"

# Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser
# Clear-Host
if ($PSVersionTable.PSVersion.Major -lt 5)
{
        Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
        exit
}
else
{
        Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}



function make_files
{
        param
        (
                [Parameter(Mandatory = $true)]
                [System.String]$input_file,
                [Parameter(Mandatory = $true)]
                [System.String]$file_destination
        )
        $current_qualys_x_csv = Import-Csv -Path $input_file     
        
        $current_qualys_x_csv | ForEach-Object {
            $existing_ips_exp = New-Object 'System.Collections.Generic.List[String]'
            $groupname = $_.TITLE
            $ip_set = $_.IP_SET
            # Make files for comparing.
            #if ($ip_set -contains ",") {
            $ip_set.Split(',') | ForEach-Object {
		    # Write-Host "POSITION ONE:  $_, $input_file"
		        if ($_.contains('-')) {
		            # It's a range
		            $start=$_.Split("-")[0].ToString()
		            $end=$_.Split("-")[1].ToString()
		            #Write-Debug "POSITION ALPHA start: $start; end: $end"
		            $rc = Get-IPrange -start $start -end $end
		            # Write-Debug "POSITION WTF rc: $rc"
		            # $existing_ips_expanded.Add($rc)
		            $rc | ForEach-Object {
		                $existing_ips_exp.Add($_)
		    		        # Write-Debug "Position two: $_"
		    	           }
		    	        } else {
		    	           # Write-Debug "POSITION BETA: $_"
		    	           $existing_ips_exp.Add($_)
		    	        }

		            }
               # } else {
               #     $existing_ips_exp = $ip_set
               #}
            # $newdir = "$fwdir\current_qualysfiles"
            $newdir = "$file_destination\current_qualysfiles"
        
            if (!(Test-Path -Path $newdir)) {
                New-Item -Path $newdir -ItemType directory
            } 
            # $existing_ips_expanded > "$newdir\$groupname.txt"
            $existing_ips_exp > "$newdir\$groupname.txt"
        }
        }


    
    # return $QualysPlatform,$HttpHeaders,$creds,$login_action,$QualysUsername,$QualysPassword
    # return $something


# make_files -input_file $old -file_destination $new

make_files -input_file $input_file -file_destination $file_destination