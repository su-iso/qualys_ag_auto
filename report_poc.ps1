#
$DebugPreference = "Continue"

# Set $script_home to the script's parent's path
$script_home = "d:\git\qualys_ag_auto"
$mydate_main = $(Get-Date -Format s)
# Remove-Item "$script_home/reports" -Force -Recurse
New-Item "$script_home/reports" -ItemType directory -Force

$QualysPassword = Get-Content -Path .\qualyspw.txt
$QualysUsername = Get-Content -Path .\qualysuser.txt
$QualysPlatform,$HttpHeaders,$creds,$login_action = .\Set-Qualys-Variables.ps1 -QualysUsername $QualysUsername -QualysPassword $QualysPassword
$session = .\Get-Qualys-Session.ps1 -QualysUsername $QualysUsername -QualysPassword $QualysPassword -Action login
$cookie = $session[1]

$report_api = "report"
$list_action = "action=list"
$fetch_action = "action=fetch"
$logout_action = "action=logout"
$report_id = $null
# $select_group =  New-Object 'System.Collections.Generic.List[String]' 

# https://qualysguard.qg2.apps.qualys.com/fo/report/report_info.php?id=3763495&view_type=0

# TODO:
# doing this manually for the POC, needs to be fixed.
$report_id = "id=3915644"
# $scorecard_id = "id="
$report_list_body = "action=list"

# $report_template_list_php =
# Get base report list
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$report_api/" -Method Post -Body $report_list_body -WebSession $cookie -OutFile "$script_home/reports/reports_list.xml"

# Get the ISO report
$report_select_body = "action=fetch&$report_id"
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$report_api/" -Method Post -Body $report_select_body -WebSession $cookie -OutFile "$script_home/reports/report_output.xml"
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $cookie

# This has all current asset group title info (and AG id)
[System.Xml.XmlDocument]$reports_xml_full = $(Get-Content -Path "$script_home/reports/reports_list.xml")
[System.Xml.XmlDocument]$reports_xml_selected = $(Get-Content -Path "$script_home/reports/report_output.xml")

[System.Collections.ArrayList]$vuln_list = $null

$all_vulns_this_round = $reports_xml_selected.ASSET_DATA_REPORT.GLOSSARY.VULN_DETAILS_LIST.VULN_DETAILS


$vuln_summary = @{}

$all_vulns_this_round| ForEach-Object {
    $sev = $_.SEVERITY
    $title = $_.TITLE.'#cdata-section'
    # $cve = $_.CVE_ID_LIST.CVE_ID.ID.'#cdata-section'
    # Write-Host "title = $title"
    if ($title.length -gt 40){
        $trunc_title = $title.substring(0,40)
    } else {
        $trunc_title = $title
    }
    $vuln_summary[$_.QID.'#text'] = ("Sev"+$sev,$trunc_title)
}


#> $testval = 11772

#> $vuln_summary.'11772'
# Sev4 [WordPress REST API User Enumeration Vulnerability] CVE-2017-5487

#> $vuln_summary."$testval"
# Sev4 [WordPress REST API User Enumeration Vulnerability] CVE-2017-5487

# TODO:  
#### $all_vulns_this_round to a dictionary of QID, Severity, and TITLE for roll-up table and Host-QID-Severity lookup

$mydate = $(Get-Date -Format s)

$report_agnames = $reports_xml_selected.ASSET_DATA_REPORT.HEADER.TARGET.USER_ASSET_GROUPS.ASSET_GROUP_TITLE.'#cdata-section'
# $report_iplist = $reports_xml_selected.ASSET_DATA_REPORT.HEADER.TARGET.COMBINED_IP_LIST.RANGE
$average_security_risk = $reports_xml_selected.ASSET_DATA_REPORT.HEADER.RISK_SCORE_SUMMARY.AVG_SECURITY_RISK # float
$total_vulns = $reports_xml_selected.ASSET_DATA_REPORT.HEADER.RISK_SCORE_SUMMARY.TOTAL_VULNERABILITIES  # int

$host_summary = $reports_xml_selected.ASSET_DATA_REPORT.HOST_LIST.HOST

$ips_with_sev3 = 0
$ips_with_sev4 = 0
$ips_with_sev5 = 0

$sev3_vulns = 0
$sev4_vulns = 0
$sev5_vulns = 0

$host_sev3_summary = @{}
$host_sev4_summary = @{}
$host_sev5_summary = @{}

# [list]$sev3_hosts = $null
# [list]$sev4_hosts = $null
# [list]$sev5_hosts = $null

$host_summary| ForEach-Object {
    [System.Collections.ArrayList]$my_sev3 = @()
    [System.Collections.ArrayList]$my_sev4 = @()
    [System.Collections.ArrayList]$my_sev5 = @()
    # [list]$myvulns = $null
    # Start-Sleep -Seconds 1
    # Hostname as index, list of vulns as value
    # $host_vuln_summary[$_.DNS.'#cdata-section'] = ("Sev"+$sev,$trunc_title)
    $my_unique_vulns = $_.VULN_INFO_LIST.VULN_INFO.QID.'#text'|Sort-Object -Unique
    $my_dns = $_.DNS.'#cdata-section'
    # Write-Host "My dns: $my_dns"
    $my_unique_vulns | ForEach-Object {
        # Write-Host $vuln_summary["$_"]
        if ($vuln_summary["$_"] -like "Sev3*") {
            $my_sev3.Add($vuln_summary["$_"])
            $sev3_vulns++
        } elseif ($vuln_summary["$_"] -like "Sev4*") {
            $my_sev4.Add($vuln_summary["$_"])
            $sev4_vulns++
        } elseif ($vuln_summary["$_"] -like "Sev5*") {
            $my_sev5.Add($vuln_summary["$_"])
            $sev5_vulns++
        }
    }
    
    if ($my_sev3.Count -gt 0){
        $ips_with_sev3++
        $host_sev3_summary["$my_dns"] = $my_sev3
    }

    if ($my_sev4.Count -gt 0){
        $ips_with_sev4++
        $host_sev4_summary["$my_dns"] = $my_sev4
    }

    if ($my_sev5.Count -gt 0){
        $ips_with_sev5++
        $host_sev5_summary["$my_dns"] = $my_sev5
        }

    # With Sev3,Sev4,Sev5 indicators
    #$host_vuln_summary[$_.DNS.'#cdata-section'] = ("Sev3:`t"+$my_sev3, "`tSev4:`t"+$my_sev4, "`tSev5:`t"+$my_sev5)
      
    

    # Without indicators
    $host_vuln_summary["$my_dns"] = ("`t`t`t`t"+$my_sev3.Count, "`t`t"+$my_sev4.Count, "`t`t"+$my_sev5.Count)
     

}
# $vulnerable_hosts_objects = $reports_xml_selected.ASSET_DATA_REPORT.HOST_LIST.HOST

# $host_vuln_summary | ForEach-Object
# Write-Host $host_vuln_summary


$number_of_vuln_hosts = $host_summary.Count
# $total_vulns = $host_summary.VULN_INFO_LIST.Count
$vulnerable_hosts = $host_summary.DNS.'#cdata-section' -join ", "

# This probably needs to be driven/extracted from an external list or DB.
$contact_list = "tsimpson@stanford.edu"

# Update this to be the business unit name
$org_name = "TODO:  link to report automation Group"

# $oldest_current_detection = $host_summary.VULN_INFO_LIST.VULN_INFO.LAST_FOUND | Sort-Object {$_ -as [DateTime]}  | Select-Object -First 1
# $newest_current_detection = $host_summary.VULN_INFO_LIST.VULN_INFO.LAST_FOUND | Sort-Object {$_ -as [DateTime]} -Descending | Select-Object -First 1


# Wrap the message in a function or if() that tests whether the variables are fresh

$hvsummary = $host_vuln_summary.GetEnumerator()| Sort-Object -Property Name| Format-Table -Property Value,Name -HideTableHeaders | Out-String

$novulns = $reports_xml_selected.ASSET_DATA_REPORT.APPENDICES.NO_VULNS.IP_LIST.RANGE.GetEnumerator()| Out-String
$noresults = $reports_xml_selected.ASSET_DATA_REPORT.APPENDICES.NO_RESULTS.IP_LIST.RANGE.GetEnumerator()|Out-String

$host_sev5_summary_output = $host_sev5_summary.GetEnumerator()| Sort-Object -Property Name| Format-Table -HideTableHeaders -AutoSize | Out-String

$host_sev5_count = $($host_sev5_summary.Keys|Sort-Object -Unique).Count
$host_sev4_count = $($host_sev4_summary.Keys|Sort-Object -Unique).Count
$host_sev3_count = $($host_sev3_summary.Keys|Sort-Object -Unique).Count


$message_text = "Hello, $contact_list!

This message is a summary of vulnerability data for your organization as currently defined in Qualys.  This information is generated via script.

Current vulnerability summary for $org_name

    Asset groups included in report: `t{ $report_agnames }


    Total count of vulnerabilities: `t$total_vulns

    Total count of vulnerable hosts: `t$number_of_vuln_hosts

    Summary of sev 5 vulnerabilities:  $host_sev5_summary_output

    Host Vulnerability summary:`n
    `t`t`t Sev3`t`t Sev4`t`tSev5 
    `t`t`t IPs:`t`t$ips_with_sev3`t`t$ips_with_sev4`t`t`t  $ips_with_sev5 
    `t`t`t Hosts:`t$host_sev3_count`t`t$host_sev4_count`t`t`t  $host_sev5_count
    `t`t`t Vulns:`t$sev3_vulns`t`t$sev4_vulns`t`t  $sev5_vulns
    `t`t`t  ---- `t`t   ----  `t`t `t ----
    $hvsummary
    `t`t`t  ---- `t`t   ----  `t`t `t ----
    `t`t`t Sev3`t`t Sev4`t`tSev5 
    `t`t`t IPs: $ips_with_sev3`t IPs: $ips_with_sev4`t`t IPs: $ips_with_sev5 


    IP ranges scanned but no selected vulnerabilities found: `n$novulns
    `nNo results from these ranges (not scanned):`n$noresults

Thank you!
ISO Reports

"
# List of vulnerable hosts: `t{ $vulnerable_hosts }

## Report lines removed:
    # Oldest of current vulnerability detections: `t$oldest_current_detection
    # Newest of current vulnerability detections: `t$newest_current_detection

Send-MailMessage -To $contact_list -From "netdb@crunchyfrog.stanford.edu" -SmtpServer "smtp-unencrypted.stanford.edu" -Subject "Qualys Executive Report" -Body $message_text