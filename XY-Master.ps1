
$credsfile = "D:\git\qualys_ag_auto\mycred.xml"
$creds = Import-Clixml -Path $credsfile
$QualysPassword = $creds.GetNetworkCredential().Password
$QualysUsername = $creds.UserName
$QualysPlatform,$HttpHeaders,$login_action = .\Set-Qualys-Variables.ps1 -QualysCredential $creds
$session = .\Get-Qualys-Session.ps1 -QualysCredential $creds -Action login
$cookie = $session[1]

# $QualysPlatform,$HttpHeaders,$creds,$login_action,$logout_action = .\Set-Qualys-Variables.ps1 -QualysUsername $QualysUsername -QualysPassword $QualysPassword
# $QualysPlatform,$HttpHeaders,$login_action,$logout_action = .\Set-Qualys-Variables.ps1

# $session = .\Get-Qualys-Session.ps1 -QualysUsername $QualysUsername -QualysPassword $QualysPassword -Action login
# $cookie = $session[1]

$fileinfo = $null
$mainfilename = $null
$filedest = $null

$fileinfo = .\Get-Qualys-AGList.ps1 -show_attributes "TITLE" -cookie $cookie -QualysCredential $creds
$mainfilename = $fileinfo

$filedest = Split-Path $fileinfo

# PS D:\git\qualys_ag_auto> $filename.FullName
# D:\git\qualys_ag_auto\firewall\csv\BD6F07E0B81AD74747A14EB5132866E3\20180712094543.csv
#
# PS D:\git\qualys_ag_auto> $filedest
# D:\git\qualys_ag_auto/firewall/csv/BD6F07E0B81AD74747A14EB5132866E3/

# Note that Build-X-Masters should not actually build the new x-master groups unless there is (any/some) new data from the networking file...
# ...but it will provide the variables needed.

$all_fws = $null
$fwdir = $null
$test_this = $null
.\Build-X-Masters.ps1

$fwdir = Get-Content "d:\git\qualys_ag_auto\firewall\fwdir.txt"

$all_firewalls = Get-Content "$fwdir\all_fw.txt"

.\Build-X-Subgroups.ps1 -allfws $all_firewalls -location $fwdir


# .\Fetch-Expand-QualysData.ps1 -mainfilename $mainfilename.FullName -cookie $cookie -fwdir $fwdir
# Logout

# .\Get-Qualys-Session.ps1 -QualysCredential $creds -logout_sess $cookie -Action logout



# $current_qualys_mg_csv = Import-Csv -Path "$fwdir/current_mg_content.csv"

# $current_mg_filelist = Get-ChildItem "$fwdir\current_qualysfiles" -Include "mg-*" -Recurse

$current_netfiles = "$fwdir\current_netfiles"
Copy-Item "D:\git\qualys_ag_auto\firewall\unfirewalled\*" -Destination $current_netfiles

# TODO: figure out a way to not have to keep this block manually updated.
# $x_foa_ips=$(Get-Content -Path "$current_netfiles/x-foa.MASTER.txt")
# $x_foa2_ips=$(Get-Content -Path "$current_netfiles/x-foa2.MASTER.txt")
# $x_doa_ips=$(Get-Content -Path "$current_netfiles/x-doa.MASTER.txt")
$x_lb_ips=$(Get-Content -Path "$current_netfiles/x-lb.MASTER.txt")
# $x_liv_ips=$(Get-Content -Path "$current_netfiles/x-liv-mgmt.MASTER.txt")
# $x_loa_ips=$(Get-Content -Path "$current_netfiles/x-loa.MASTER.txt")
# $x_woa_ips=$(Get-Content -Path "$current_netfiles/x-woa.MASTER.txt")
$x_mc_ips=$(Get-Content -Path "$current_netfiles/x-mc.MASTER.txt")
$x_mn_ips=$(Get-Content -Path "$current_netfiles/x-mn.MASTER.txt")
$x_nc_ips=$(Get-Content -Path "$current_netfiles/x-nc.MASTER.txt")
# $x_rcf_ips=$(Get-Content -Path "$current_netfiles/x-rcf.MASTER.txt")
$x_sc_ips=$(Get-Content -Path "$current_netfiles/x-sc.MASTER.txt")
$x_wc_ips=$(Get-Content -Path "$current_netfiles/x-wc.MASTER.txt")
$x_cef_ips=$(Get-Content -Path "$current_netfiles/x-cef.MASTER.txt")
$x_dc1_ips=$(Get-Content -Path "$current_netfiles/x-dc1.MASTER.txt")
$x_dc2_ips=$(Get-Content -Path "$current_netfiles/x-dc2.MASTER.txt")
$x_dc3_ips=$(Get-Content -Path "$current_netfiles/x-dc3.MASTER.txt")
$x_rwc_cef_ips=$(Get-Content -Path "$current_netfiles/x-rwc-cef.MASTER.txt")
$x_rwc_mn_ips=$(Get-Content -Path "$current_netfiles/x-rwc-mn.MASTER.txt")
$x_rwc_ips=$(Get-Content -Path "$current_netfiles/x-rwc.MASTER.txt")
# $x_asprod_vsys80_ips=$(Get-Content -Path "$current_netfiles/x-asprod-vsys80.txt")
# $x_psoft_vsys55_ips=$(Get-Content -Path "$current_netfiles/x-psoft-vsys55.txt")
# $x_bigfix_vsys5_ips=$(Get-Content -Path "$current_netfiles/x-bigfix-vsys5.txt")
# $x_stg_mgm_vsys4_ips=$(Get-Content -Path "$current_netfiles/x-stg-mgm-vsys4.txt")
$uf_10_ips = $(Get-Content -Path "$current_netfiles/x-10-unfirewalled.txt")
$uf_128_ips = $(Get-Content -Path "$current_netfiles/x-128-unfirewalled.txt")
$uf_171_ips = $(Get-Content -Path "$current_netfiles/x-171-unfirewalled.txt")
$uf_172_ips = $(Get-Content -Path "$current_netfiles/x-172-unfirewalled.txt")
$uf_192_ips = $(Get-Content -Path "$current_netfiles/x-192-unfirewalled.txt")
$uf_204_ips = $(Get-Content -Path "$current_netfiles/x-204-unfirewalled.txt")


## User-defined `mg-` groups are gathered to make `sg-` subgroups 
#$current_qualys_mg_csv | ForEach-Object { 
#	# Operating on one master asset group at a time
#    $id=$_.ID
#	$title=$_.TITLE
#	$ouid="null"
#	$ouid=$_.OWNER_USER_ID
#	$title_translated=$title.Replace(" ","_")
#	$agtitle=$title_translated -replace('^mg-','')
#	$agname=$title_translated -replace('^mg-','sg-')
#    $filematch = $( $current_mg_filelist | Where-Object {$_.Name -eq "$title.txt"} )	
#    $existing_ips_expanded = $(Get-content $filematch.FullName)
#	$existing_ips_expanded | ForEach-Object {
#		$subagname = "NULL"
#		# Start-Sleep -s 2
#		$ip = $_.ToString()
#		# Write-Debug "AG_name: $agname [$ip]"
#		if ($uf_10_ips -Contains "$ip") {
#			$subagname = "$agname-uf10"
#		}
#        elseif ($x_nc_ips -Contains "$ip") {
#			$subagname = "$agname-NC"
#		}
#		elseif ($x_mc_ips -Contains "$ip") {
#			$subagname = "$agname-MC"
#		} 
#		elseif ($x_wc_ips -Contains "$ip") {
#			$subagname = "$agname-WC"
#		} 
#		elseif ($x_asprod_vsys80_ips -Contains "$ip") {
#			$subagname = "$agname-asprod-vsys80"
#		} 
#		elseif ($x_stg_mgm_vsys4_ips -Contains "$ip") {
#			$subagname = "$agname-stg-mgm-vsys4"
#		} 
#		elseif ($x_psoft_vsys55_ips -Contains "$ip") {
#			$subagname = "$agname-psoft-vsys55"
#		} 
#		elseif ($x_bigfix_vsys5_ips -Contains "$ip") {
#			$subagname = "$agname-bigfix-vsys5"
#		} 
#		elseif ($x_foa2_ips -Contains "$ip") {
#			$subagname = "$agname-FOA2"
#		} 
#		elseif ($x_foa_ips -Contains "$ip") {
#			$subagname = "$agname-FOA"
#		} 
#		elseif ($x_doa_ips -Contains "$ip") {
#			$subagname = "$agname-DOA"
#		} *BuThat
#		# elseif ($x_moa_ips -Contains "$ip") {
#		#	$subagname = "$agname-MOA"
#		#} 
#		#elseif ($x_noa_ips -Contains "$ip") {
#		#    $subagname = "$agname-NOA"
#		#} 
#		elseif ($x_woa_ips -Contains "$ip") {
#			$subagname = "$agname-WOA"
#		} else {
#			$subagname = "$agname-special"
#		}
#	
#	# Write a file with the name $subagname--$ouid containing all the enumerated IPs in $existing_ips_expanded
#	$_ | Out-File -NoClobber -Append -FilePath "$current_netfiles\$subagname--$ouid" -Encoding ascii
#	}
#}

# $userlogin_info = Import-Csv "./current-q-users-outfile.txt"
# $robologins = Import-Csv "./logins/isorobot_userlogins.csv"

# Copy-Item "D:\git\qualys_ag_auto\firewall\unfirewalled" -Destination $current_netfiles

$new_files = $(Get-ChildItem -Force "$current_netfiles" -File)
$mainfile = $(Get-content $fileinfo.FullName)

$mainfile[1..($mainfile.Count-2)] > temp_file
$main_content = Import-Csv temp_file
Remove-Item temp_file
$asset_group_api = "asset/group"

$new_files | ForEach-Object {
    $action = "null"
 	$agid = $null
    $ag_ouid = $null
    $this_filename = $_.Name
    $this_filepath = $_.FullName
    if ($this_filename -notlike "sg-*") {
        # Everthing else should be x-groups 
        $agname = $_.Name -replace (".txt","")
     }
    

    $new_ips_expanded = Get-Content -Path "$this_filepath" 
    $new_ips=$new_ips_expanded -join ","

    #Check to see if the agname already exists in Qualys data;  if so, grab the agid
    $main_content | ForEach-Object {
        if ($_.TITLE -eq $agname){
            $agid = $_.ID
            $action = "edit"
            # break
            }
    }

    if ($action -eq "edit" ) {
        $action_asset_group_body = "action=edit&id=$agid&set_ips=$new_ips"
    } else {
        $action_asset_group_body = "action=add&title=$agname&ips=$new_ips"
    }

    # Write-Host "tlogin:  $tlogin, tpass: $tpass"
	# Work slowly to be a good API consumer.
    Start-Sleep -Seconds 1

    # if ($tlogin -ne $null -and $tpass -ne $null) {

        # This is an x-group
        Write-Host "IN THE ELSE"
        # Start-Sleep -Seconds 5
        # $QualysPassword = Get-Content -Path .\qualyspw.txt
        # $QualysUsername = Get-Content -Path .\qualysuser.txt
#         $credsfile = "D:\git\qualys_ag_auto\mycred.xml"
#        $creds = Import-Clixml -Path $credsfile
#        $QualysPassword = $creds.GetNetworkCredential().Password
#        $QualysUsername = $creds.UserName
#        $QualysPlatform,$HttpHeaders,$login_action,$logout_action = .\Set-Qualys-Variables.ps1 -QualysCredential $creds
 
        # $nsession = .\Get-Qualys-Session.ps1 -QualysCredential $creds -Action login
        # $ncookie = $nsession[1]
        # Start-Sleep -Seconds 1
        ## $QualysPlatform,$HttpHeaders,$creds,$login_action,$logout_action = .\Set-Qualys-Variables.ps1 -QualysUsername $QualysUsername -QualysPassword $QualysPassword
        # $QualysPlatform,$HttpHeaders,$login_action,$logout_action = .\Set-Qualys-Variables.ps1 -QualysCredential $creds
        ## $nsession = .\Get-Qualys-Session.ps1 -QualysUsername $QualysUsername -QualysPassword $QualysPassword -Action login
        # $nsession = .\Get-Qualys-Session.ps1 -QualysCredential $creds -Action login
        # $ncookie = $nsession[1]
        Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $action_asset_group_body -WebSession $cookie -OutFile "$script_home/check_x_group_add.txt"
        # .\Get-Qualys-Session.ps1 -QualysCredential $creds -Action logout -logout_sess $ncookie
     }
.\Get-Qualys-Session.ps1 -QualysCredential $creds -Action logout -logout_sess $cookie
