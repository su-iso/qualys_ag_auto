#
$DebugPreference = "Continue"

# Set $script_home to the script's parent's path
$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$mydate_main = $(Get-Date -Format s)


$most_recent_network_run_dir = Get-ChildItem $script_home/firewall/ -Directory| Sort-Object -Property LastWriteTime| Select-Object -last 1
$last_files_uploaded = "$script_home/firewall/$most_recent_network_run_dir/upload"
$xlation = "$last_files_uploaded/xlation"
$new_upload = "$last_files_uploaded/new_upload"

Remove-Item "$xlation" -Recurse
Remove-Item "$new_upload" -Recurse
New-Item "$xlation" -ItemType directory -Force
New-Item "$new_upload" -ItemType directory -Force
$master_filenames = New-Object 'System.Collections.Generic.List[String]'
Get-ChildItem $last_files_uploaded -Name | grep -i master| ForEach-Object {$master_filenames.Add($_)}

# Variable $master_files now has all the master filenames as a list.

# Source this local .ps1 file.  This allows for easy range expansion of ranges received from Qualys.
. "$script_home/Get-IPrange.ps1"

$summary_list = New-Object 'System.Collections.Generic.List[String]'

# Grab these groups as 'master' groups - we'll make the 'sg-' groups from these
# $ags_to_parse = @("mg-*","AS High Risk Servers")
$ags_to_parse = @("mg-*")

#TODO:  on windows, fix this to be an actual secure call
# Get a qualys session, get a list of asset groups, destroy the session
# Set up qualys API login info.   NOTE these first two lines will be different per user/group.
$QualysUserName = $(get-Content -Path "$script_home/qualysuser.txt")
$QualysPassword = $(get-Content -Path "$script_home/qualyspw.txt")

$QualysPlatform = 'https://qualysapi.qg2.apps.qualys.com/api/2.0/fo'
# v1 api
# $QualysAPI = 'https://qualysapi.qg2.apps.qualys.com/msp/ticket_list.php?'
$BasicAuthString = [System.Text.Encoding]::UTF8.GetBytes("$QualysUserName`:$QualysPassword")

# $BasicAuthBase64Encoded = [System.Convert]::ToBase64String($BasicAuthString)
$password_base64 = ConvertTo-SecureString $QualysPassword -AsPlainText -Force  
$creds = New-Object System.Management.Automation.PSCredential ($QualysUserName, $password_base64) 

$BasicAuthFormedCredential = "Basic $creds"
$HttpHeaders = @{'Authorization' = $BasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}


# API actions and endpoints
$login_action = "action=login&username=$QualysUserName&password=$QualysPassword"
$logout_action = "action=logout"

$asset_group_api = "asset/group"
$all_asset_titles_id = "action=list&show_attributes=TITLE&output_format=csv"
$select_group =  New-Object 'System.Collections.Generic.List[String]' 


##### Available data fields for asset groups
### ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
### HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME

# Log in to Qualys.
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Credential $creds -Body $login_action -SessionVariable sess

# Get Asset Group list into a csv file.
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $all_asset_titles_id -WebSession $sess -OutFile "$script_home/check_ag.csv"


# This has all current asset group title info (and AG id)
$check_csv_full = $(Get-Content -Path "$script_home/check_ag.csv")

$my_groups = New-Object 'System.Collections.Generic.List[String]' 
$my_ids = New-Object 'System.Collections.Generic.List[String]' 
$my_objects = New-Object 'System.Collections.ArrayList'

# First output line is the indicator of success!
$lineone = $check_csv_full[0]
# Write-Debug "check_csv_full[0]: $lineone"
if ($lineone -like "----BEGIN_RESPONSE_BODY_CSV") { 
   # This approach includes the header line, which is usually line 2 of the response at least for asset group output
   $check_csv_full[1..($check_csv_full.Count-2)] > "$script_home/response_content.csv"
   # $response_content becomes the reference point for all AG > ID lookups
   $response_content = Import-Csv "$script_home/response_content.csv" 
   $select_ids = New-Object 'System.Collections.Generic.List[String]' 
   
   $response_content | ForEach-Object {
			$id=$_.ID
			$title=$_.TITLE
			# Write-Debug "my_xyz_groups:  $title,$id" 
			$my_groups.Add("$title")
			$my_ids.Add("$id")
			$my_objects.Add("$_")
			# Magic string for selecting asset groups is "ag_"
			# if ($title -like "ag_*")
			$ags_to_parse | ForEach-Object {
				# Write-Debug "ag_pattern:  $_, title: $title, id:  $id"
				# Start-Sleep -s 1
				# if ($_ -like $title) {
				if ($title -like $_) {
					Write-Debug "_ is $_:: title is: $title :: id is: $id"
					$select_ids.Add("$id")
				}
				# Write-Debug "select_ids:  $select_ids"
			}
			#if ($ags_to_parse -Contains $title){
			#    $select_ids.Add("$id")
			#}
  }
  # Get the extended data just for the Asset Groups we care about
  $joined_ids = $select_ids -join ","
  
  
  ################
  ###-----> Valid attributes for request of Asset groups in Qualys v2 api:  
  ###
  # ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
  # HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME
  #
  ####################################################

  # $groups_to_parse_body = "action=list&ids=$joined_ids&show_attributes=ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS&output_format=csv"
  $groups_to_parse_body = "action=list&ids=$joined_ids&show_attributes=ID,TITLE,IP_SET,OWNER_UNIT_ID,OWNER_USER_ID,APPLIANCE_LIST,COMMENTS&output_format=csv"


  # Write-Debug "#### CURRENT:  my_y_groups:  $my_y_groups"
  # Write-Debug "Get info for selected asset groups"
  Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $groups_to_parse_body -WebSession $sess -OutFile "$script_home/selected_ag_check.csv"
  Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $sess
  
  } 



# Get the current asset group info from csv
$current_group_info = $(Get-Content -Path "$script_home/selected_ag_check.csv")

# Get just the content
$current_group_info[1..($current_group_info.Count-2)] > "$script_home/current_group_info.csv"
$current_group_content = Import-Csv "$script_home/current_group_info.csv" -Delimiter ","

$current_checkrange = New-Object 'System.Collections.ArrayList'

$current_group_content | ForEach-Object { 
	# Operating on one master asset group at a time
	$existing_ips_expanded = New-Object 'System.Collections.Generic.List[String]'
	$id=$_.ID
	$title=$_.TITLE
	$ouid="null"
	$ouid=$_.OWNER_USER_ID
	$title_translated=$title.Replace(" ","_")
	$agtitle=$title_translated -replace('^mg-','')
	$old_ips=$($_.IP_SET)
	$old_ips.Split(',') | ForEach-Object {
		# Write-Debug "POSITION ONE:  $_"
		if ($_.contains('-')) { 
		   # It's a range
		   $start=$_.Split("-")[0].ToString()
		   $end=$_.Split("-")[1].ToString()
		   # Write-Debug "POSITION ALPHA start: $start; end: $end"
		   $rc = Get-IPrange -start $start -end $end
		   # Write-Debug "POSITION WTF rc: $rc"
		   $rc | ForEach-Object {
			$existing_ips_expanded.Add($_)
			# Write-Debug "Position two: $_"
		   }
		$comments=$_.COMMENTS

		} else { $existing_ips_expanded.Add($_)}
	}
	# Write a file with the name sg-$title.txt containing all the enumerated IPs in $existing_ips_expanded
	$existing_ips_expanded > $xlation/sg-$agtitle--$ouid.txt
}



# TODO: figure out a way to not have to keep this block manually updated.
$x_foa_ips=$(Get-Content -Path "$last_files_uploaded/x-foa-srtr.MASTER.txt")
$x_foa2_ips=$(Get-Content -Path "$last_files_uploaded/x-foa2-srtr.MASTER.txt")
$x_doa_ips=$(Get-Content -Path "$last_files_uploaded/x-doa-srtr.MASTER.txt")
# $x_moa_ips=$(Get-Content -Path "$last_files_uploaded/x-moa-srtr.MASTER.txt")
$x_noa_ips=$(Get-Content -Path "$last_files_uploaded/x-noa-srtr.MASTER.txt")
$x_woa_ips=$(Get-Content -Path "$last_files_uploaded/x-woa-srtr.MASTER.txt")
$x_mc_ips=$(Get-Content -Path "$last_files_uploaded/x-mc-srtr.MASTER.txt")
$x_nc_ips=$(Get-Content -Path "$last_files_uploaded/x-nc-srtr.MASTER.txt")
$x_wc_ips=$(Get-Content -Path "$last_files_uploaded/x-wc-srtr.MASTER.txt")
$x_asprod_vsys80_ips=$(Get-Content -Path "$last_files_uploaded/x-asprod-vsys80.txt")
$x_psoft_vsys55_ips=$(Get-Content -Path "$last_files_uploaded/x-psoft-vsys55.txt")
$x_bigfix_vsys5_ips=$(Get-Content -Path "$last_files_uploaded/x-bigfix-vsys5.txt")
$x_stg_mgm_vsys4_ips=$(Get-Content -Path "$last_files_uploaded/x-stg-mgm-vsys4.txt")


# Get list of upload files

$files = $(Get-ChildItem -Force "$xlation" -File)

# for each of the files
$files | ForEach-Object {
        $name = $_.Name
	# get asset group name
	$agname_with_ouid = $_.Name -replace '\.txt$',''
	$agname = $agname_with_ouid.Split("--")[0]
	$ag_ouid = $agname_with_ouid.Split("--")[1]

	$new_ips_expanded = Get-Content -Path "$xlation/$name" 

	# Check each IP in the main 'sg-' group and parse into OA-specific subgroup
	$new_ips_expanded | ForEach-Object {
		$subagname = "NULL"
		# Start-Sleep -s 2
		$ip = $_.ToString()
        	# Write-Debug "AG_name: $agname [$ip]"
		if ($x_foa_ips -Contains "$ip") {
			$subagname = "$agname-FOA"
		} 
		elseif ($x_foa2_ips -Contains "$ip") {
			$subagname = "$agname-FOA2"
		} 
		elseif ($x_doa_ips -Contains "$ip") {
			$subagname = "$agname-DOA"
		} 
		elseif ($x_asprod_vsys80_ips -Contains "$ip") {
			$subagname = "$agname-asprod-vsys80"
		} 
		elseif ($x_stg_mgm_vsys4_ips -Contains "$ip") {
			$subagname = "$agname-stg-mgm-vsys4"
		} 
		elseif ($x_psoft_vsys55_ips -Contains "$ip") {
			$subagname = "$agname-psoft-vsys55"
		} 
		elseif ($x_bigfix_vsys5_ips -Contains "$ip") {
			$subagname = "$agname-bigfix-vsys5"
		} 
		elseif ($x_mc_ips -Contains "$ip") {
			$subagname = "$agname-MC"
		} 
		elseif ($x_nc_ips -Contains "$ip") {
			$subagname = "$agname-NC"
		} 
		elseif ($x_wc_ips -Contains "$ip") {
			$subagname = "$agname-WC"
		} 
		elseif ($x_moa_ips -Contains "$ip") {
			$subagname = "$agname-MOA"
		} 
		elseif ($x_noa_ips -Contains "$ip") {
			$subagname = "$agname-NOA"
		} 
		elseif ($x_woa_ips -Contains "$ip") {
			$subagname = "$agname-WOA"
		} else {
			$subagname = "$agname-special"
		}
        	# Write-Debug "sub_agname: $subagname [$_]"
        	# Write-Debug "sub_agname: $subagname [$ip]"
		$_ | Out-File -NoClobber -Append -FilePath "$new_upload/$subagname--$ag_ouid" -Encoding ascii
	}
}

$new_files = $(Get-ChildItem -Force "$new_upload" -File)

$new_files | ForEach-Object {
 	$agid = $null
        $name = $_.Name
	$agname_with_ouid = $_.Name -replace (".txt","")
	$agname = $agname_with_ouid.Split("--")[0]
	$ag_ouid = $agname_with_ouid.Split("--")[1]
	$new_ips_expanded = Get-Content -Path "$new_upload/$name" 
 	$new_ips=$new_ips_expanded -join ","
 	# Check whether AG already exists
 		# If AG already exists, grab its ID
 		# It's an edit operation!  We need an asset group ID for this to work.
 		# Write-Debug "my_xyz_objects = $my_xyz_objects"
 		$matching_ag_id = $null
 		$old_ips = $null
 		$comments = $null
 		$existing_ips_expanded = New-Object 'System.Collections.Generic.List[String]'
 		# $new_ips| ForEach-Object {(Invoke-PSipcalc -NetworkAddress $_ -Enumerate).IPEnumerated}
 
 		# This will be the whole object which matches the AG name
 		$chosen = $($current_group_content| Where-Object {$_.TITLE -eq "$agname"})
 		$matching_ag_id = $($chosen.ID)
 		$agid = $($chosen.ID)
 		
 		if ($matching_ag_id -ne $null) {
 			$old_ips=$($chosen.IP_SET)
 			$old_ips.Split(',') | ForEach-Object {
 			        # Write-Debug "POSITION ONE:  $_"
 				if ($_.contains('-')) {
 				   # It's a range
 				   $start=$_.Split("-")[0].ToString()
 				   $end=$_.Split("-")[1].ToString()
 				   # Write-Debug "POSITION ALPHA start: $start; end: $end"
 				   $rc = Get-IPrange -start $start -end $end 
 				   # Write-Debug "POSITION WTF rc: $rc"
 				   $rc | ForEach-Object {
 				        $existing_ips_expanded.Add($_)
 					# Write-Debug "Position two: $_"
 				   }
 				} else {
 				   # Write-Debug "POSITION BETA: $_"
 				   $existing_ips_expanded.Add($_)
 				}
 
 			}
 
 			Write-Debug "###################### UPGRAAAAADE! Name $agname found!!! Existing AG $agname will need to be updated. ID is $matching_ag_id"
 
 			# Using Get-Date for now since the comments field is too small for diffs
 			$comments = "Updated:  $(Get-Date -Format s)"
 
 		# https://stackoverflow.com/questions/19012457/compare-two-list-and-find-names-that-are-in-list-one-and-not-list-two-using-powe
 		$to_remove=$($existing_ips_expanded | ?{$new_ips_expanded -notcontains $_})
 		$check_new_vs_old=$($new_ips_expanded |?{$existing_ips_expanded -notcontains $_})
 		$check_overlap=$($new_ips_expanded | ?{$existing_ips_expanded -contains $_})

 		$to_remove_Count=$to_remove.Count
 		$check_new_vs_old_Count=$check_new_vs_old.Count
 		$check_overlap_Count=$check_overlap.Count
		$ips_count=$existing_ips_expanded.Count
 		# Write-Debug "----------- to_remove:  $to_remove"

		Start-Sleep -s 2
 		if ($to_remove_Count -gt 0 -Or $check_new_vs_old_Count -gt 0 -And $ips_count -gt 0) {
 			Write-Debug "----------- to_remove: $to_remove_Count"
 			Write-Debug "----------- check_new_vs_old: $check_new_vs_old_Count"
 			Write-Debug "----------- total new ips: $ips_count"
 			# Write-Debug "----------- check_overlap_Count:  $check_overlap_Count"
 			$action_asset_group_body = "action=edit&id=$matching_ag_id&set_ips=$new_ips&set_comments=$comments"
 			# $action_ag_post_check = "action=edit&id=$matching_ag_id&set_ips=$ips&set_comments=$comments"
 		} else {
 		$action_asset_group_body = "action=list&ids=$matching_ag_id"
 		Write-Debug "===== continue - no change section: should only hit here if new, remove, and payload are all 0"
 		}
 		# Write-Debug "----------- check_overlap: $check_overlap_Count"
 		# Write-Debug "action=edit&id=$matching_ag_id&set_ips=$ips&set_comments=$comments"
 		} else {
 		# Should be a new asset group name
 		Write-Debug "Name $agname will be a new AG."
 		$action_asset_group_body = "action=add&title=$agname&ips=$new_ips"
  		}
 
 	$action_aftercheck = "action=list&ids=$agid&show_attributes=ID,TITLE,IP_SET&output_format=csv"
 
 
 	###### uncomment to actually upload / change asset groups
 	# The actual qualys call to add/update the AG

	# asdf This is where we add the code to switch to a BU-specific user to run the AG updates

	# At this point we have 
	# 	- $agid: asset group id
	# 	- $ag_ouid: asset group owner user id
	# 	- $agname:  AG title/name
	$login_info = Import-Csv "./current-q-users-outfile.txt"

	$tbusiness_unit = $null
	$login_info | ForEach-Object {
		$user_id = $_.USER_ID
		if ($user_id -eq $ag_ouid) {
			$tbusiness_unit = $_.BUSINESS_UNIT
		} else {
			return 1
		}
	}

	Write-Debug "----------- tbusiness_unit: $tbusiness_unit"

	$robologins = Import-Csv "./logins/isorobot_userlogins.csv"
	$tlogin = $null
	$tpass = $null


	$robologins | ForEach-Object {
		if ($_.Name -eq $tbusiness_unit) {
			$tlogin = $_.login
			$tpass = $_.pass
		} else {
			return 1
		}
	}
	Write-Debug "----------- tlogin: $tlogin"
	# Write-Debug "----------- $tbusiness_unit: $tbusiness_unit"

	####### make the creds from the temp login/pass
	# $tQualysUserName = $(get-Content -Path "$script_home/qualysuser.txt")
	# $tQualysPassword = $(get-Content -Path "$script_home/qualyspw.txt")

	# $tBasicAuthString = [System.Text.Encoding]::UTF9.GetBytes("$tlogin`:$tpass")

	$tpassword_base64 = ConvertTo-SecureString $tpass -AsPlainText -Force  
	$tcreds = New-Object System.Management.Automation.PSCredential ($tlogin, $tpassword_base64) 

	$tBasicAuthFormedCredential = "Basic $tcreds"
	$tHttpHeaders = @{'Authorization' = $tBasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}

	####### make the creds from the temp login/pass ^
	$login_action = "action=login&username=$tlogin&password=$tpass"

  	Invoke-RestMethod -Headers $tHttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Credential $tcreds -Body $login_action -SessionVariable tsess
 	Invoke-RestMethod -Headers $tHttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $action_asset_group_body -WebSession $tsess -OutFile "$script_home/check_asset_group_add.txt"
 
 	# Start-Sleep -s 2
 
 
   	Invoke-RestMethod -Headers $tHttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $action_aftercheck -WebSession $tsess -OutFile "$script_home/reparse_aftercheck_y.csv"
        Invoke-RestMethod -Headers $tHttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $tsess

 	$post_info = $(Get-Content -Path "$script_home/reparse_aftercheck_y.csv")
 	$post_info[2..($post_info.Count-2)] > "$script_home/reparse_aftercheck_xyz_info.csv"
 	$post_headervals = $post_info[1]
 	$post_xyz_headers = $post_headervals.Split(",").Replace("`"","")
 	$post_xyz_content = Import-Csv "$script_home/reparse_aftercheck_xyz_info.csv" -Header $post_xyz_headers -Delimiter ","
 	$post_action_aglist = New-Object 'System.Collections.Generic.List[String]'
 	$rangeset = New-Object 'System.Collections.Generic.List[String]'
 	$ipset = New-Object 'System.Collections.Generic.List[String]'
 
 	# add expanded output.Count from post-check
 	$post_xyz_content | ForEach-Object {
 		# $ipset=$_.IP_SET.ToString()
 		# $ipset=$_.IP_SET.Split(',')
 		$ipset=$_.IP_SET
 		# Write-Debug "POSITION ONE:  $ipset.split(',')"
 		if ($ipset -notcontains(',')) {
 			# Write-Debug "POSITION FUUUUU:  $ipset"
 			$rangeset.Add($ipset)
 			} else {
 			$ipset.Split(',') | ForEach-Object {
 			# Write-Debug "POSITION FUUUUU:  $ipset"
 			$rangeset.Add($_)
 			}
 		}
 		# Write-Debug "POSITION ONE:  $ipset"
 		# Write-Debug "POSITION ONE:  "+$ipset.split(',')
 		$rangeset| ForEach-Object {
 			if ($_.contains('-')) {
 			   $start=$_.Split("-")[0].ToString()
 			   $end=$_.Split("-")[1].ToString()
 			   # Write-Debug "POSITION ALPHA start: $start; end: $end"
 			   $rc = Get-IPrange -start $start -end $end
 			   # Write-Debug "POSITION WTF rc: $rc"
 			   # $existing_ips_expanded.Add($rc)
 			   $rc | ForEach-Object {
 				$post_action_aglist.Add($_)
 				# Write-Debug "Position two: $_"
 				}
 			} else {
 		   # Write-Debug "POSITION BETA: $_"
 		   # $existing_ips_expanded.Add($_.IP_SET)
 		   $post_action_aglist.Add($_.IP_SET)
 		}
 
 	}
 	$tmp_count=$post_action_aglist.Count
         # Write-Debug "post_y_content $agname, ip count: $tmp_count"
 	# Write-Debug "#######----------- to_remove: $to_remove_Count"
 	$this_round = @($agname,$tmp_count,$to_remove_Count,$check_new_vs_old_Count,$check_overlap_Count)
 	$summary_list.Add("$this_round")
 	# Write-Debug "Summary list: $summary_list"
 	$this_round -join "," >> "$xlation/summary_$mydate_main.csv" 
 	}
 
 # Log out of Qualys.
 Write-Debug "Logging out of Qualys"
 # Write-Debug "logout_action:  $logout_action"
 # Write-Debug "sess:  $sess"
 
 $mydate = $(Get-Date -Format s)
 
 # asdf
 $summary_list | Export-Csv -Path "$xlation/summary_$mydate.csv" 
}
 
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $sess

