param (
        [Parameter(Mandatory = $False)]
        [System.String]$ag_search_string_list = "x-*",
        [Parameter(Mandatory = $False)]
        [System.String]$show_attributes = "TITLE",
        [Parameter(Mandatory = $True)]
        [Microsoft.PowerShell.Commands.WebRequestSession]$cookie,
        [Parameter(Mandatory = $True)]
        [System.String]$input_file,
        [Parameter(Mandatory = $True)]
        [System.String]$destination_directory
)

$xyz_search = $ag_search_string_list

$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$asset_group_api = "asset/group"
# $all_asset_titles_id = "action=list&show_attributes=TITLE&output_format=csv"

# $selected_xyz_asset_titles_body = "action=list&ids=$my_xyzids&show_attributes=ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS&output_format=csv"
# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $selected_xyz_asset_titles_body -WebSession $sess -OutFile "$script_home/firewall/selected_xyz_check_ag.csv"

# $all_asset_titles_id = "action=list&show_attributes=$show_attributes&output_format=csv"
# $selected_asset_titles_id = "action=list&ids=$ag_id_list&show_attributes=$show_attributes&output_format=csv"


$select_group =  New-Object 'System.Collections.Generic.List[String]' 

##### Available data fields for asset groups
### ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
### HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME


#PS D:\git\qualys_ag_auto> $session_out = .\Get-Qualys-Session.ps1 -QualysUserName sunet2ma -QualysPassword $QualysPassword -Action login
#PS D:\git\qualys_ag_auto> $cookie = $session_out[1]
#PS D:\git\qualys_ag_auto> Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $all_asset_titles_id -WebSession $cookie -OutFile "D:\git\qualys_ag_auto\test_outfile.txt"
#PS D:\git\qualys_ag_auto> .\Get-Qualys-Session.ps1 -QualysUserName sunet2ma -QualysPassword $QualysPassword -Action logout -logout_sess $cookie

$DebugPreference = "Continue"
[Microsoft.PowerShell.Commands.WebRequestSession]$sess=$null
#[Microsoft.PowerShell.Commands.WebRequestSession]$session=$null
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser
# Clear-Host
if ($PSVersionTable.PSVersion.Major -lt 5)
{
        Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
        exit
}
else
{
        # Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}


function get_qualys_aglist
{
        param
        (
                [Parameter(Mandatory = $true)]
                [Microsoft.PowerShell.Commands.WebRequestSession] $cookie,
                [Parameter(Mandatory = $true)]
                $body_value,
                [Parameter(Mandatory = $True)]
                [System.String]$destination_directory
        )

    # Get Asset Group list into a csv file.
    $date = $(Get-Date -UFormat "%Y%m%d%H%M%S")
    $filename = "$date.csv"
    # $outfile = "$script_home\firewall\csv\$destination_directory\$filename"
    $outfile = "$script_home\firewall\$destination_directory\$filename"
    Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $body_value -WebSession $cookie -OutFile $outfile
	#return [Microsoft.PowerShell.Commands.WebRequestSession]$session
    return $outfile
}

# $check_csv_full = $(Get-Content -Path "$script_home/firewall/$input_file")
Write-Host "Input file:  $input_file"
$check_csv_full = $(Get-Content -Path "$input_file")

$my_xyz_ids = New-Object 'System.Collections.Generic.List[String]'
$my_xyzids = New-Object 'System.Collections.Generic.List[String]'
$ag_list = New-Object 'System.Collections.Generic.List[String]'

# First output line is the indicator of success!
$lineone = $check_csv_full[0]
Write-Debug "check_csv_full[0]: $lineone"
if ($lineone -like "----BEGIN_RESPONSE_BODY_CSV") 
   { 
   # This approach includes the header line, which is usually line 2 of the response at least for asset group output
   $check_csv_full[1..($check_csv_full.Count-2)] > "$script_home/firewall/response_content.csv"
   # $response_content becomes the reference point for all AG > ID lookups
   $response_content = Import-Csv "$script_home\firewall\response_content.csv" 

   # $response_content = Import-Csv -Path $input_file
   
   $response_content | ForEach-Object {
			$id=$_.ID
			$title=$_.TITLE
			 
            if ($title -like $xyz_search) {
                    # Write-Debug "my_groups:  $title,$id"
	    	        $my_xyz_ids.Add("$id")
                }
			}
  # Get the extended data just for the Asset Groups we care about
  $my_xyzids = $my_xyz_ids -join ","


  ################
  ###-----> Valid attributes for request of Asset groups in Qualys v2 api:  
  ###
  # ID, TITLE, OWNER_USER_ID, OWNER_UNIT_ID, LAST_UPDATE, IP_SET, APPLIANCE_LIST, DOMAIN_LIST, \
  # HOST_IDS, ASSIGNED_USER_IDS, ASSIGNED_UNIT_IDS, BUSINESS_IMPACT, COMMENTS, OWNER_USER_NAME
  #
  ####################################################

  # $selected_xyz_asset_titles_body = "action=list&ids=$my_xyzids&show_attributes=ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS&output_format=csv"
  # $show_attributes = "ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS"

  Write-Debug "#### CURRENT:  my_xyzids:  $my_xyzids, $xyz_search"
  # Write-Debug "Get info for selected asset groups"
  # Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $selected_xyz_asset_titles_body -WebSession $sess -OutFile "$script_home/firewall/selected_xyz_check_ag.csv"
  $ag_list = .\Get-Qualys-AGList.ps1 -show_attributes $show_attributes -ag_id_list $my_xyzids -cookie $cookie
  # return $ag_list,$filedest
  return $ag_list
  } else {return "NULLL"}