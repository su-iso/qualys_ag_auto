#
param (
	[Parameter(Mandatory = $false)]
 	[System.String]$qualysuserid,
 	[Parameter(Mandatory = $false)]
 	[System.String]$qualyspwd,
 	[Parameter(Mandatory = $false)]
 	[Boolean]$debugmsgs = $false
 )

# $DebugPreference = "Continue"
# $QualysUserName = $(get-Content -Path "$script_home/qualysuser.txt")
# $QualysPassword = $(get-Content -Path "$script_home/qualyspw.txt")

$script_home = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent

# Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser
# Clear-Host
if ($PSVersionTable.PSVersion.Major -lt 5)
{
	Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
	exit
}
else
{
	Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}

function b64encodepwd
{
	[CmdletBinding(SupportsShouldProcess = $false)]
	[OutputType('System.String')]
	param
	(
		[Parameter(Mandatory = $true)]
		[System.String]$id,
		[Parameter(Mandatory = $true)]
		[System.String]$pwd
	)
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    $EncodedPwd = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $id, $pwd)))
    Remove-Variable id
    Remove-Variable pwd
	#[System.String]$EncodedPwd = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes("$($id):$($pwd)"))
	if ($debugmsgs)
	{
		write-warning "DERBERG:  $debugmsgs"
	}
	return $EncodedPwd
}

function encodepwd
{
	[CmdletBinding(SupportsShouldProcess = $false)]
	[OutputType('System.String')]
	param
	(
		[Parameter(Mandatory = $true)]
		[System.String]$id,
		[Parameter(Mandatory = $true)]
		[System.String]$pwd
	)
	[System.String]$EncodedPwd = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes("$($id):$($pwd)"))
	if ($debugmsgs)
	{
		write-warning "`tUser ID $($id) with Password $($pwd) Encoded as $($EncodedPwd)"
	}
	return $EncodedPwd
}

function fetchqualysuserdata
{
	[CmdletBinding(SupportsShouldProcess = $false)]
	[OutputType('System.Xml.XmlDocument')]
	param
	(
		[Parameter(Mandatory = $true)]
		$EncodedPassword
	)
	[System.Collections.Hashtable]$headers = @{
		Authorization			= "Basic $EncodedPassword";
		'X-Requested-With'		= "PowerShell Script"
	}
	[System.Xml.XmlDocument]$RawData = Invoke-WebRequest -Uri https://qualysapi.qg2.apps.qualys.com/msp/user_list.php -Headers $headers
	return [System.Xml.XmlDocument]$RawData
}
function strippedConvertFrom-xml-to-psobjects
{
	[CmdletBinding(SupportsShouldProcess = $false)]
	[OutputType('System.Array')]
	param
	(
		[Parameter(Mandatory = $true)]
		[System.Xml.XmlDocument]$data
	)
	[System.Array]$ObjArray = @()
	if ($debugmsgs) { Write-Warning "Begin Data Parsing" }
	[int]$Users = $data.USER_LIST_OUTPUT.USER_LIST.USER.count
	[int]$Index = 1
	foreach ($user_rec in $data.USER_LIST_OUTPUT.USER_LIST.USER)
	{
		Write-Progress -id 1 -Activity "Parsing Qualys Users" -SecondsRemaining -1 -PercentComplete (($Index++/$Users) * 100) -Status "Parsing info for user $($user_rec.USER_LOGIN)" -ErrorAction SilentlyContinue
		$ObjArray += New-Object System.Management.Automation.PSObject -Property @{
			#BUSINESS_UNIT  = $user_rec.BUSINESS_UNIT.InnerText
			USER_LOGIN	   = $user_rec.USER_LOGIN
			USER_ID	       = $user_rec.USER_ID
			EXTERNAL_ID    = $user_rec.EXTERNAL_ID.InnerText
			EMAIL		   = $user_rec.CONTACT_INFO.EMAIL.InnerText
			USER_STATUS    = $user_rec.USER_STATUS
			LAST_LOGIN_DATE = $user_rec.LAST_LOGIN_DATE
			USER_ROLE	   = $user_rec.USER_ROLE
			BUSINESS_UNIT  = $user_rec.BUSINESS_UNIT.InnerText
			UNIT_MANAGER_POC = $user_rec.UNIT_MANAGER_POC
			MANAGER_POC    = $user_rec.MANAGER_POC
			TIME_ZONE_CODE = $user_rec.TIME_ZONE_CODE
		}
		if ($debugmsgs)
		{
			Write-Warning "`tUSER_LOGIN`t=>$($user_rec.USER_LOGIN)"
			Write-Warning "`tUSER_ID`t=>$($user_rec.USER_ID)"
			Write-Warning "`tEXTERNAL_ID`t=>$($user_rec.EXTERNAL_ID.InnerText)"
			Write-Warning "`tEMAIL`t=>$($user_rec.CONTACT_INFO.EMAIL.InnerText)"
			Write-Warning "`tUSER_STATUS`t=>$($user_rec.USER_STATUS)"
			Write-Warning "`tCREATION_DATE`t=>$($user_rec.CREATION_DATE)"
			Write-Warning "`tLAST_LOGIN_DATE`t=>$($user_rec.LAST_LOGIN_DATE)"
			Write-Warning "`tUSER_ROLE`t=>$($user_rec.USER_ROLE)"
			Write-Warning "`tBUSINESS_UNIT`t=>$($user_rec.BUSINESS_UNIT.InnerText)"
			Write-Warning "`tUNIT_MANAGER_POC`t=>$($user_rec.UNIT_MANAGER_POC)"
			Write-Warning "`tMANAGER_POC`t=>$($user_rec.MANAGER_POC)"
		}
	}
	if ($debugmsgs) { Write-Warning "End Data Parsing" }
	return [System.Array]$ObjArray | ConvertTo-Csv
}

function ConvertFrom-xml-to-psobjects
{
	[CmdletBinding(SupportsShouldProcess = $false)]
	[OutputType('System.Array')]
	param
	(
		[Parameter(Mandatory = $true)]
		[System.Xml.XmlDocument]$data
	)
	[System.Array]$ObjArray = @()
	if ($debugmsgs) { Write-Warning "Begin Data Parsing" }
	[int]$Users = $data.USER_LIST_OUTPUT.USER_LIST.USER.count
	[int]$Index = 1
	foreach ($user_rec in $data.USER_LIST_OUTPUT.USER_LIST.USER)
	{
		Write-Progress -id 1 -Activity "Parsing Qualys Users" -SecondsRemaining -1 -PercentComplete (($Index++/$Users) * 100) -Status "Parsing info for user $($user_rec.USER_LOGIN)" -ErrorAction SilentlyContinue
		$ObjArray += New-Object System.Management.Automation.PSObject -Property @{
			USER_LOGIN	   = $user_rec.USER_LOGIN
			USER_ID	       = $user_rec.USER_ID
			EXTERNAL_ID    = $user_rec.EXTERNAL_ID.InnerText
			FIRSTNAME	   = $user_rec.CONTACT_INFO.FIRSTNAME.InnerText
			LASTNAME	   = $user_rec.CONTACT_INFO.LASTNAME.InnerText
			TITLE		   = $user_rec.CONTACT_INFO.TITLE.InnerText
			PHONE		   = $user_rec.CONTACT_INFO.PHONE.InnerText
			FAX		       = $user_rec.CONTACT_INFO.FAX.InnerText
			EMAIL		   = $user_rec.CONTACT_INFO.EMAIL.InnerText
			COMPANY	       = $user_rec.CONTACT_INFO.COMPANY.InnerText
			ADDRESS1	   = $user_rec.CONTACT_INFO.ADDRESS1.InnerText
			ADDRESS2	   = $user_rec.CONTACT_INFO.ADDRESS2.InnerText
			CITY		   = $user_rec.CONTACT_INFO.CITY.InnerText
			COUNTRY	       = $user_rec.COUNTRY
			STATE		   = $user_rec.STATE
			ZIP_CODE	   = $user_rec.ZIP_CODE
			TIME_ZONE_CODE = $user_rec.TIME_ZONE_CODE.InnerText
			USER_STATUS    = $user_rec.USER_STATUS
			CREATION_DATE  = $user_rec.CREATION_DATE
			LAST_LOGIN_DATE = $user_rec.LAST_LOGIN_DATE
			USER_ROLE	   = $user_rec.USER_ROLE
			BUSINESS_UNIT  = $user_rec.BUSINESS_UNIT.InnerText
			UNIT_MANAGER_POC = $user_rec.UNIT_MANAGER_POC
			MANAGER_POC    = $user_rec.MANAGER_POC
			PERMISSIONS_CREATE_OPTION_PROFILES = $user_rec.PERMISSIONS.CREATE_OPTION_PROFILES
			PERMISSIONS_PURGE_INFO = $user_rec.PERMISSIONS.PURGE_INFO
			PERMISSIONS_ADD_ASSETS = $user_rec.PERMISSIONS.ADD_ASSETS
			PERMISSIONS_EDIT_REMEDIATION_POLICY = $user_rec.PERMISSIONS.EDIT_REMEDIATION_POLICY
			PERMISSIONS_EDIT_AUTH_RECORDS = $user_rec.PERMISSIONS.EDIT_AUTH_RECORDS
		}
		if ($debugmsgs)
		{
			Write-Warning "`tUSER_LOGIN`t=>$($user_rec.USER_LOGIN)"
			Write-Warning "`tUSER_ID`t=>$($user_rec.USER_ID)"
			Write-Warning "`tEXTERNAL_ID`t=>$($user_rec.EXTERNAL_ID.InnerText)"
			Write-Warning "`tFIRSTNAME`t=>$($user_rec.CONTACT_INFO.FIRSTNAME.InnerText)"
			Write-Warning "`tLASTNAME`t=>$($user_rec.CONTACT_INFO.LASTNAME.InnerText)"
			Write-Warning "`tTITLE`t=>$($user_rec.CONTACT_INFO.TITLE.InnerText)"
			Write-Warning "`tPHONE`t=>$($user_rec.CONTACT_INFO.PHONE.InnerText)"
			Write-Warning "`tFAX`t=>$($user_rec.CONTACT_INFO.FAX.InnerText)"
			Write-Warning "`tEMAIL`t=>$($user_rec.CONTACT_INFO.EMAIL.InnerText)"
			Write-Warning "`tCOMPANY`t=>$($user_rec.CONTACT_INFO.COMPANY.InnerText)"
			Write-Warning "`tADDRESS1`t=>$($user_rec.CONTACT_INFO.ADDRESS1.InnerText)"
			Write-Warning "`tADDRESS2`t=>$($user_rec.CONTACT_INFO.ADDRESS2.InnerText)"
			Write-Warning "`tCITY`t=>$($user_rec.CONTACT_INFO.CITY.InnerText)"
			Write-Warning "`tCOUNTRY`t=>$($user_rec.COUNTRY)"
			Write-Warning "`tSTATE`t=>$($user_rec.STATE)"
			Write-Warning "`tZIP_CODE`t=>$($user_rec.ZIP_CODE)"
			Write-Warning "`tTIME_ZONE_CODE`t=>$($user_rec.TIME_ZONE_CODE.InnerText)"
			Write-Warning "`tUSER_STATUS`t=>$($user_rec.USER_STATUS)"
			Write-Warning "`tCREATION_DATE`t=>$($user_rec.CREATION_DATE)"
			Write-Warning "`tLAST_LOGIN_DATE`t=>$($user_rec.LAST_LOGIN_DATE)"
			Write-Warning "`tUSER_ROLE`t=>$($user_rec.USER_ROLE)"
			Write-Warning "`tBUSINESS_UNIT`t=>$($user_rec.BUSINESS_UNIT.InnerText)"
			Write-Warning "`tUNIT_MANAGER_POC`t=>$($user_rec.UNIT_MANAGER_POC)"
			Write-Warning "`tMANAGER_POC`t=>$($user_rec.MANAGER_POC)"
			Write-Warning "`tPERMISSIONS CREATE_OPTION_PROFILES`t=>$($user_rec.PERMISSIONS.CREATE_OPTION_PROFILES)"
			Write-Warning "`tPERMISSIONS PURGE_INFO`t=>$($user_rec.PERMISSIONS.PURGE_INFO)"
			Write-Warning "`tPERMISSIONS ADD_ASSETS`t=>$($user_rec.PERMISSIONS.ADD_ASSETS)"
			Write-Warning "`tPERMISSIONS EDIT_REMEDIATION_POLICY`t=>$($user_rec.PERMISSIONS.EDIT_REMEDIATION_POLICY)"
			Write-Warning "`tPERMISSIONS EDIT_AUTH_RECORDS`t=>$($user_rec.PERMISSIONS.EDIT_AUTH_RECORDS)"
		}
	}
	if ($debugmsgs) { Write-Warning "End Data Parsing" }
	return [System.Array]$ObjArray
}

function Main
{

# $QualysUserName = $(get-Content -Path "$script_home/qualysuser.txt")
# $QualysPassword = $(get-Content -Path "$script_home/qualyspw.txt")

# $QualysPlatform = 'https://qualysapi.qg2.apps.qualys.com/api/2.0/fo'
# $BasicAuthString = [System.Text.Encoding]::UTF8.GetBytes("$QualysUserName`:$QualysPassword")

# # $BasicAuthBase64Encoded = [System.Convert]::ToBase64String($BasicAuthString)
# $password_base64 = ConvertTo-SecureString $QualysPassword -AsPlainText -Force
# $creds = New-Object System.Management.Automation.PSCredential ($QualysUserName, $password_base64)

# $BasicAuthFormedCredential = "Basic $creds"
# $HttpHeaders = @{'Authorization' = $BasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}


	$qualysuserid = Get-Content -Path "d:\git\qualys_ag_auto\qualysuser.txt"
	$qualyspwd = Get-Content -Path "d:\git\qualys_ag_auto\qualyspw.txt"

	$BasicAuthString = [System.Text.Encoding]::UTF8.GetBytes("$qualysuserid`:$qualyspwd")

	# $password_base64 = ConvertTo-SecureString $QualysPassword -AsPlainText -Force
	$password_base64 = ConvertTo-SecureString $qualyspwd -AsPlainText -Force
	$creds = New-Object System.Management.Automation.PSCredential ($qualysuserid, $password_base64)

	$BasicAuthFormedCredential = "Basic $creds"
	$HttpHeaders = @{'Authorization' = $BasicAuthFormedCredential; 'X-Requested-With' = 'PowerShell Script'}

	# Write-Host "user id = $qualysuserid"
	# Write-Debug "pwd = $qualyspwd"
	$EncodedPassword = b64encodepwd -id $qualysuserid -pwd $qualyspwd
	# [System.String]$EncodedPassword = $creds
	[System.Xml.XmlDocument]$RawData = fetchqualysuserdata -EncodedPassword $EncodedPassword

	# [System.Array]$MyUserObjs = ConvertFrom-xml-to-psobjects -data $RawData
	[System.Array]$MyUserObjs = strippedConvertFrom-xml-to-psobjects -data $RawData

	$MyUserObjs > current-q-users-outfile.txt

}

. Main

# Invoke-Expression "$script_home/high_risk_reparse.ps1"
