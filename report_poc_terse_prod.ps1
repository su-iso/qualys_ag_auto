#

param(
$input_list="tsimpson@stanford.edu",
[Int]$report_id_int=4041390,
[Int]$iso_mode='0'
)
$DebugPreference = "Continue"

# Set $script_home to the script's parent's path
$script_home = "d:\git\qualys_ag_auto"
$mydate_main = $(Get-Date -Format s)
# Remove-Item "$script_home/reports" -Force -Recurse
New-Item "$script_home/reports" -ItemType directory -Force

$credsfile = "D:\git\qualys_ag_auto\qualys_creds.xml"
$creds = Import-Clixml -Path $credsfile
$QualysPassword = $creds.GetNetworkCredential().Password
$QualysUsername = $creds.UserName
$QualysPlatform,$HttpHeaders,$login_action = .\Set-Qualys-Variables.ps1 -QualysCredential $creds
$session = .\Get-Qualys-Session.ps1 -QualysCredential $creds -Action login
$cookie = $session[1]


# $QualysPassword = Get-Content -Path .\qualyspw.txt
# $QualysUsername = Get-Content -Path .\qualysuser.txt
# $QualysPlatform,$HttpHeaders,$creds,$login_action = .\Set-Qualys-Variables.ps1 -QualysUsername $QualysUsername -QualysPassword $QualysPassword
# $session = .\Get-Qualys-Session.ps1 -QualysUsername $QualysUsername -QualysPassword $QualysPassword -Action login
# $cookie = $session[1]

$report_api = "report"
$list_action = "action=list"
$fetch_action = "action=fetch"
$logout_action = "action=logout"
$report_id = $null
# $select_group =  New-Object 'System.Collections.Generic.List[String]' 

# https://qualysguard.qg2.apps.qualys.com/fo/report/report_info.php?id=3763495&view_type=0

# TODO:
# doing this manually for the POC, needs to be fixed.
# $contact_list = @("tsimpson@stanford.edu") 
# $contact_list = @("it.management@law.stanford.edu")
$report_id = "id=$report_id_int"
# $scorecard_id = "id="
$report_list_body = "action=list"

# $report_template_list_php =
# Get base report list
# Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$report_api/" -Method Post -Body $report_list_body -WebSession $cookie -OutFile "$script_home/reports/reports_list.xml"

# Get the ISO report
$report_select_body = "action=fetch&$report_id"
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$report_api/" -Method Post -Body $report_select_body -WebSession $cookie -OutFile "$script_home/reports/report_output.xml"
Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $cookie

# This has all current asset group title info (and AG id)
[System.Xml.XmlDocument]$reports_xml_full = $(Get-Content -Path "$script_home/reports_list.xml")
[System.Xml.XmlDocument]$reports_xml_selected = $(Get-Content -Path "$script_home/reports/report_output.xml")

[System.Collections.ArrayList]$vuln_list = $null

$all_vulns_this_round = $reports_xml_selected.ASSET_DATA_REPORT.GLOSSARY.VULN_DETAILS_LIST.VULN_DETAILS


$vuln_summary = @{}

$all_vulns_this_round| ForEach-Object {
    $sev = $_.SEVERITY
    $title = $_.TITLE.'#cdata-section'
    # $cve = $_.CVE_ID_LIST.CVE_ID.ID.'#cdata-section'
    # Write-Host "title = $title"
    if ($title.length -gt 40){
        $trunc_title = $title.substring(0,40)
    } else {
        $trunc_title = $title
    }
    $vuln_summary[$_.QID.'#text'] = ("Sev"+$sev,$trunc_title)
}


#> $testval = 11772

#> $vuln_summary.'11772'
# Sev4 [WordPress REST API User Enumeration Vulnerability] CVE-2017-5487

#> $vuln_summary."$testval"
# Sev4 [WordPress REST API User Enumeration Vulnerability] CVE-2017-5487

# TODO:  
#### $all_vulns_this_round to a dictionary of QID, Severity, and TITLE for roll-up table and Host-QID-Severity lookup

$mydate = $(Get-Date -Format s)

$report_agnames = $reports_xml_selected.ASSET_DATA_REPORT.HEADER.TARGET.USER_ASSET_GROUPS.ASSET_GROUP_TITLE.'#cdata-section'
# $report_iplist = $reports_xml_selected.ASSET_DATA_REPORT.HEADER.TARGET.COMBINED_IP_LIST.RANGE
$average_security_risk = $reports_xml_selected.ASSET_DATA_REPORT.HEADER.RISK_SCORE_SUMMARY.AVG_SECURITY_RISK # float
$total_vulns = 0
$total_vulns = $reports_xml_selected.ASSET_DATA_REPORT.HEADER.RISK_SCORE_SUMMARY.TOTAL_VULNERABILITIES  # int

$host_summary = $reports_xml_selected.ASSET_DATA_REPORT.HOST_LIST.HOST

$ips_with_sev3 = 0
$ips_with_sev4 = 0
$ips_with_sev5 = 0

$sev3_vulns = 0
$sev4_vulns = 0
$sev5_vulns = 0

$host_sev3_summary = @{}
$host_sev4_summary = @{}
$host_sev5_summary = @{}

$host_vuln_summary = @{}

# [list]$sev3_hosts = $null
# [list]$sev4_hosts = $null
# [list]$sev5_hosts = $null

$host_summary| ForEach-Object {
    [System.Collections.ArrayList]$my_sev3 = @()
    [System.Collections.ArrayList]$my_sev4 = @()
    [System.Collections.ArrayList]$my_sev5 = @()
    # [list]$myvulns = $null
    # Start-Sleep -Seconds 1
    # Hostname as index, list of vulns as value
    # $host_vuln_summary[$_.DNS.'#cdata-section'] = ("Sev"+$sev,$trunc_title)
    # $my_unique_vulns = $_.VULN_INFO_LIST.VULN_INFO.QID.'#text'|Sort-Object -Unique
    $my_unique_vulns = $_.VULN_INFO_LIST.VULN_INFO.QID.'#text'
    $my_dns = $_.DNS.'#cdata-section'
    # Write-Host "My dns: $my_dns"
    $my_unique_vulns | ForEach-Object {
        # Write-Host $vuln_summary["$_"]
        if ($vuln_summary["$_"] -like "Sev3*") {
            $my_sev3.Add($vuln_summary["$_"])
            $sev3_vulns++
        } elseif ($vuln_summary["$_"] -like "Sev4*") {
            $my_sev4.Add($vuln_summary["$_"])
            $sev4_vulns++
        } elseif ($vuln_summary["$_"] -like "Sev5*") {
            $my_sev5.Add($vuln_summary["$_"])
            $sev5_vulns++
        }
    }
    
    if ($my_sev3.Count -gt 0){
        $ips_with_sev3++
        $host_sev3_summary["$my_dns"] = $my_sev3
    }

    if ($my_sev4.Count -gt 0){
        $ips_with_sev4++
        $host_sev4_summary["$my_dns"] = $my_sev4
    }

    if ($my_sev5.Count -gt 0){
        $ips_with_sev5++
        $host_sev5_summary["$my_dns"] = $my_sev5
        }

    # With Sev3,Sev4,Sev5 indicators
    #$host_vuln_summary[$_.DNS.'#cdata-section'] = ("Sev3:`t"+$my_sev3, "`tSev4:`t"+$my_sev4, "`tSev5:`t"+$my_sev5)
      
    $sev3count = $my_sev3.Count
    $sev4count = $my_sev4.Count
    $sev5count = $my_sev5.Count

    # Without indicators
    $host_vuln_summary["$my_dns"] = ("`t`t`t`t"+$sev3count, "`t`t"+$sev4count, "`t`t"+$sev5count)
     

}
# $vulnerable_hosts_objects = $reports_xml_selected.ASSET_DATA_REPORT.HOST_LIST.HOST

# $host_vuln_summary | ForEach-Object
# Write-Host $host_vuln_summary


# for later deduplication
$vulnerable_hosts_uniq = $host_summary.DNS.'#cdata-section' | sort| Get-Unique 
$vulnerable_hosts = $vulnerable_hosts_uniq -join ", "
$number_of_vuln_hosts = $vulnerable_hosts_uniq.Count

# $vulnerable_hosts_select = $host_summary.DNS.'#cdata-section' 
# $vulnerable_hosts = $vulnerable_hosts_select -join ", "
# $number_of_vuln_hosts = $vulnerable_hosts_select.Count

# This probably needs to be driven/extracted from an external list or DB.
# $contact_list = @("mjduff@stanford.edu","pingwei@stanford.edu","timineri@stanford.edu","bhavya14@stanford.edu","perillo@stanford.edu","tsimpson@stanford.edu")
# $contact_list = @("tsimpson@stanford.edu","pingwei@stanford.edu")




# Update this to be the business unit name
$org_name = "TODO:  link to report automation Group"

# $oldest_current_detection = $host_summary.VULN_INFO_LIST.VULN_INFO.LAST_FOUND | Sort-Object {$_ -as [DateTime]}  | Select-Object -First 1
# $newest_current_detection = $host_summary.VULN_INFO_LIST.VULN_INFO.LAST_FOUND | Sort-Object {$_ -as [DateTime]} -Descending | Select-Object -First 1


# Wrap the message in a function or if() that tests whether the variables are fresh

$hvsummary = $host_vuln_summary.GetEnumerator()| Sort-Object -Property Name| Format-Table -Property Value,Name -HideTableHeaders | Out-String

$novulns = $reports_xml_selected.ASSET_DATA_REPORT.APPENDICES.NO_VULNS.IP_LIST.RANGE.GetEnumerator()| Out-String
$noresults = $reports_xml_selected.ASSET_DATA_REPORT.APPENDICES.NO_RESULTS.IP_LIST.RANGE.GetEnumerator()|Out-String

$host_sev5_summary_output = $host_sev5_summary.GetEnumerator()| Sort-Object -Property Name| Format-Table -HideTableHeaders -AutoSize | Out-String

$host_sev5_count = $($host_sev5_summary.Keys|Sort-Object -Unique).Count
$host_sev4_count = $($host_sev4_summary.Keys|Sort-Object -Unique).Count
$host_sev3_count = $($host_sev3_summary.Keys|Sort-Object -Unique).Count


$vuln_summary_count = $vuln_summary.Count


###############################################################################
####### Before editing any of the content or layout of the table / email below, 
####### please be sure to get the explicit approval of Michael Duff.  The content
####### below was approved on Aug 1, 2018.

$annotation = 'N/A'
$email_subject = "System Vulnerability Executive Report"
$contact_list = new-object System.Net.Mail.MailAddressCollection

if ($number_of_vuln_hosts -eq 0) {
    $total_vulns = 0
}

if ($iso_mode -eq 1) {
        $annotation = $null
        # $contact_list = "iso-qualys-reports@lists.stanford.edu"
        $contact_list.Add('iso-qualys-reports@lists.stanford.edu')
        $note = "Target mail list: $input_list`n"
        $note += "Asset groups:  $report_agnames"
        $annotation = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($note))
        $email_subject = "ISO-ONLY DRAFT: Vulnerability Executive Report"
    } elseif ($iso_mode -eq 2) {
        $annotation = $null
        # $iso_list=@('pingwei@stanford.edu, tsimpson@stanford.edu, ywillis@stanford.edu')
        $iso_list=@('tsimpson@stanford.edu, ywillis@stanford.edu')
        $iso_list -split "," -replace "\s",""| ForEach-Object {$contact_list.Add($_)}
        $note = "------------- Report $report_id  -----<br>"
        $note += "Target mail list: $input_list<br>"
        $note += "Report title: $thistitle<br>"
        $annotation = $note
        $email_subject = "TKS: ISO-ONLY DRAFT: Vulnerability Executive Report"
    } elseif ($iso_mode -eq 0) {
        $annotation = $null
        $note = "------------- Report $report_id  -----<br>"
        $note += "Target mail list: $input_list<br>"
        $note += "Report title: $thistitle<br>"
        $annotation = $note
        $input_list -split "," -replace "\s",""| ForEach-Object {$contact_list.Add($_)}
    }


$table="<style type=`"text/css`">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-us36{border-color:inherit;text-align:center}
</style>
<table class=`"tg`">
  <tr>
    <th class=`"tg-us36`"> Results </th>
    <th class=`"tg-us36`">Severity 3</th>
    <th class=`"tg-us36`">Severity 4</th>
    <th class=`"tg-us36`">Severity 5</th>
  </tr>"

$table += "<tr><td class=`"tg-us36`">Vulnerable Systems</td>
    <td class=`"tg-us36`">" + $host_sev3_count + " </td>
    <td class=`"tg-us36`">" + $host_sev4_count + " <br></td>
    <td class=`"tg-us36`">" + $host_sev5_count + " </td>
  </tr>"

$table += "<tr><td class=`"tg-us36`">Total Vulnerability Count</td>
    <td class=`"tg-us36`">" + $sev3_vulns + " </td>
    <td class=`"tg-us36`">" + $sev4_vulns + " <br></td>
    <td class=`"tg-us36`">" + $sev5_vulns + " </td>
  </tr></table>"


$message_text = "Hello,<br><br>

Below is your automated monthly vulnerability scan results summary.
Please note that your system administrators receive a detailed report.<br><br>
Our scanning identified $total_vulns confirmed Severity 3/4/5 vulnerabilities across $number_of_vuln_hosts systems in your organization.<br><br>




$table
<br>
Thank you,<br>
Information Security Office
<br>
<br>
$annotation
"


####### Before editing any of the content or layout of the table / email above, 
####### please be sure to get the explicit approval of Michael Duff.  The content
####### above was approved on Aug 1, 2018.
###############################################################################



# List of vulnerable hosts: `t{ $vulnerable_hosts }

## Report lines removed:
    # Oldest of current vulnerability detections: `t$oldest_current_detection
    # Newest of current vulnerability detections: `t$newest_current_detection
    #    $hvsummary
    #`t`t`t  ---- `t`t   ----  `t`t `t ----
    #`t`t`t Sev3`t`t Sev4`t`tSev5 
    #`t`t`t IPs: $ips_with_sev3`t IPs: $ips_with_sev4`t`t IPs: $ips_with_sev5 
    # `t`t`t`t    IPs:`t`t $ips_with_sev3`t`t  $ips_with_sev4`t`t  $ips_with_sev5 

#    Summary of sev 5 vulnerabilities:  $host_sev5_summary_output

# Current vulnerability summary for $org_name
# 
# Asset groups included in report: `t{ $report_agnames }
# 
# Total count of vulnerabilities ------> $total_vulns
# 
# Total count of vulnerable hosts ---> $number_of_vuln_hosts


# Vulnerability summary:
# 
# `t`t`tSeverity 3 - Severity 4 - Severity 5 
# `t`tHosts:`t   $host_sev3_count`t`t  $host_sev4_count`t`t  $host_sev5_count
#    Vulnerabilities:`t$sev3_vulns`t`t$sev4_vulns`t`t$sev5_vulns

    #IP ranges scanned but no selected vulnerabilities found: `n$novulns
    #`nNo results from these ranges (not scanned):`n$noresults



Write-Host "Sending mail to:  $contact_list after a 7s sleep."
Start-Sleep -Seconds 7 -Verbose

# Production run - set iso mode to 0
Send-MailMessage -To "$contact_list" -Bcc "iso-qualys-reports@lists.stanford.edu" -From "iso-qualys-reports@lists.stanford.edu" -SmtpServer "smtp-unencrypted.stanford.edu" -Subject $email_subject -Body $message_text -BodyAsHtml

# Test mode - set iso mode to 2
# Send-MailMessage -To $contact_list -From "iso-qualys-reports@lists.stanford.edu" -SmtpServer "smtp-unencrypted.stanford.edu" -Subject $email_subject -Body $message_text -BodyAsHtml

