param (
        [Parameter(Mandatory = $True)]
        [System.Management.Automation.PSCredential]$QualysCredential,
        [Parameter(Mandatory = $True)]
        [System.String]$Action = 'logout',
        [Parameter(Mandatory = $False)]
        [Microsoft.PowerShell.Commands.WebRequestSession]$logout_sess
)


#PS D:\git\qualys_ag_auto> $session_out = .\Get-Qualys-Session.ps1 -QualysUserName sunet2ma -QualysPassword $QualysPassword -Action login
#PS D:\git\qualys_ag_auto> $cookie = $session_out[1]
#PS D:\git\qualys_ag_auto> Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/$asset_group_api/" -Method Post -Body $all_asset_titles_id -WebSession $cookie -OutFile "D:\git\qualys_ag_auto\test_outfile.txt"
#PS D:\git\qualys_ag_auto> .\Get-Qualys-Session.ps1 -QualysUserName sunet2ma -QualysPassword $QualysPassword -Action logout -logout_sess $cookie

$DebugPreference = "Continue"
# [Microsoft.PowerShell.Commands.WebRequestSession]$session=$null
#[Microsoft.PowerShell.Commands.WebRequestSession]$session=$null
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser
# Clear-Host
if ($PSVersionTable.PSVersion.Major -lt 5)
{
        Write-Error "Minimum Version of Powershell is version 5 you have Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) Installed; try to install: https://www.microsoft.com/en-us/download/details.aspx?id=54616"
        exit
}
else
{
        Write-Warning "Powershell Version $($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor) detected; we can run!"
}
#function encodepwd
#{
#        # [CmdletBinding(SupportsShouldProcess = $false)]
#        [OutputType('System.String')]
#        param
#        (
#                [Parameter(Mandatory = $true)]
#                [System.String]$id,
#                [Parameter(Mandatory = $true)]
#                [System.String]$pwd
#        )
#        [System.String]$EncodedPwd = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes("$($id):$($pwd)"))
#        if ($debugmsgs)
#        {
#                write-warning "`tUser ID $($id) with Password $($pwd) Encoded as $($EncodedPwd)"
#        }
#        return $EncodedPwd
#}



function get_qualys_session
{
        #param
        #(
        #        [Microsoft.PowerShell.Commands.WebRequestSession] $session
        #)
    # Write-Debug "Variables: $login_action"
    # Write-Debug "Variables:  $HttpHeaders, $QualysPlatform, $creds, $login_action"
  	Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Credential $creds -Body $login_action -SessionVariable session
	#return [Microsoft.PowerShell.Commands.WebRequestSession]$session
    return $session
}

function logout_qualys_session
{
        param
        (
                $logout_sess
        )
    # $session.Cookies.Add($session_cookie)   
    Invoke-RestMethod -Headers $HttpHeaders -Uri "$QualysPlatform/session/" -Method Post -Body $logout_action -WebSession $logout_sess
    # return [Microsoft.PowerShell.Commands.WebRequestSession]$sess
}


$QualysPlatform,$HttpHeaders,$login_action,$logout_action=D:\git\qualys_ag_auto\Set-Qualys-Variables.ps1 -QualysCredential $QualysCredential

# get_qualys_session
# return $sess

# Write-Debug "QualysPlatform = $QualysPlatform"

if ($Action -ne 'logout') {
   # [Microsoft.PowerShell.Commands.WebRequestSession]$session = get_qualys_session
   $session = get_qualys_session
   return $session
} else {
   logout_qualys_session($logout_sess)
}
