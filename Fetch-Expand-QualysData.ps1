﻿param (
        [Parameter(Mandatory = $True)]
        [System.String]$mainfilename,
        [Parameter(Mandatory = $True)]
        [System.String]$fwdir,
        [Parameter(Mandatory = $True)] 
        [Microsoft.PowerShell.Commands.WebRequestSession]$cookie
        # [Parameter(Mandatory = $True)]
        # [System.String]$fwdir
)

# $range_expand_lockfile = "$fwdir\RANGE_EXPAND_LOCKFILE"

 
# New-Item "$range_expand_lockfile" -ItemType File -Force

# TODO:  add code for fetching net2oa.txt from afs or web.  Currently this file is manually moved to crunchyfrog.
# Note that Build-X-Masters should not actually build the new x-master groups unless there is (any/some) new data from the networking file...
# ...but it will provide the variables needed.
# $fwdir,$all_fws = .\Build-X-Masters.ps1
# the above should already be calling Build-X-Subgroups as necessary, and produces all the 'new' or 'proposed' groups in $fwdir

$x_groups_current = .\Get-Matching-Asset-Groups.ps1 -show_attributes "ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS" -cookie $cookie -input_file $mainfilename -destination_directory $fwdir -ag_search_string_list "x-*"
# PS D:\git\qualys_ag_auto> $x_groups[0].FullName
# D:\git\qualys_ag_auto\firewall\csv\684EFC4B03AF958C32CC72431F7624DA\20180712130908.csv

Write-Host "X-groups current:  $x_groups_current"

$xcontent = Get-Content $x_groups_current.FullName

$current_x_content = $xcontent[1..($xcontent.Count-2)]
$current_x_content > "$fwdir\current_x_content.csv" # (old or existing files)

# $current_qualys_x_csv = Import-Csv -Path "$fwdir\current_x_content.csv"

# This next bit takes a long time, and depends on the input file
.\Make-Files-From-Qualys-CSV.ps1 -input_file "$fwdir\current_x_content.csv" -file_destination $fwdir

######
$mg_groups_current = .\Get-Matching-Asset-Groups.ps1 -show_attributes "ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS,OWNER_USER_ID" -cookie $cookie -input_file $mainfilename -destination_directory $fwdir -ag_search_string_list "mg-*"
Write-Host "MG-groups current:  $mg_groups_current"

# $content = Get-content $mg_groups_current[0].FullName

$mgcontent = Get-content $mg_groups_current.FullName

$current_mg_content = $mgcontent[1..($mgcontent.Count-2)]
$current_mg_content > "$fwdir\current_mg_content.csv" # (old or existing files)

# $current_qualys_mg_csv = Import-Csv -Path "$fwdir/current_mg_content.csv"
.\Make-Files-From-Qualys-CSV.ps1 -input_file "$fwdir\current_mg_content.csv" -file_destination $fwdir

######
# $SG_groups_current  = .\Get-Matching-Asset-Groups.ps1 -show_attributes "ID,TITLE,IP_SET,APPLIANCE_LIST,COMMENTS,OWNER_USER_ID" -cookie $cookie -input_file $mainfilename -destination_directory $fwdir -ag_search_string_list "sg-*"

# $content = Get-Content $SG_groups_current[0].FullName
#3$sgcontent = Get-Content $SG_groups_current[0]

# $current_SG_content = $sgcontent[1..($sgcontent.Count-2)]
# $current_SG_content > "$fwdir\current_SG_content.csv" # (old or existing files)

# $current_qualys_SG_csv = Import-Csv -Path "$fwdir/current_SG_content.csv"
# .\Make-Files-From-Qualys-CSV.ps1 -input_file "$fwdir\current_SG_content.csv" -file_destination $fwdir

# Remove-Item "$range_expand_lockfile" -Force